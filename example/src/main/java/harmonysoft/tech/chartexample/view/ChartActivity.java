package harmonysoft.tech.chartexample.view;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import harmonysoft.tech.chartexample.R;
import harmonysoft.tech.chartexample.data.ChartDatasHolder;
import harmonysoft.tech.chartexample.view.flow.FlowLayout;
import harmonysoft.tech.chartexample.view.scroll.ScrollManager;
import tech.harmonysoft.android.leonardo.model.config.LeonardoConfigFactory;
import tech.harmonysoft.android.leonardo.model.config.axis.AxisConfig;
import tech.harmonysoft.android.leonardo.model.config.axis.impl.TimeAxisLabelTextStrategy;
import tech.harmonysoft.android.leonardo.model.config.chart.ChartConfig;
import tech.harmonysoft.android.leonardo.model.config.chart.ChartConfigProvider;
import tech.harmonysoft.android.leonardo.model.config.navigator.NavigatorConfig;
import tech.harmonysoft.android.leonardo.model.config.navigator.NavigatorConfigProvider;
import tech.harmonysoft.android.leonardo.model.data.ChartData;
import tech.harmonysoft.android.leonardo.model.data.ChartDataSource;
import tech.harmonysoft.android.leonardo.model.runtime.ChartModel;
import tech.harmonysoft.android.leonardo.model.runtime.impl.ChartModelImpl;
import tech.harmonysoft.android.leonardo.model.style.ReloadableStyleable;
import tech.harmonysoft.android.leonardo.util.LeonardoUtil;
import tech.harmonysoft.android.leonardo.view.chart.ChartView;
import tech.harmonysoft.android.leonardo.view.navigator.NavigatorChartView;
import tech.harmonysoft.android.leonardo.view.navigator.ScrollListener;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.Collection;

public class ChartActivity extends Activity {

    private final Collection<ReloadableStyleable> mStyleables = new ArrayList<>();

    private ScrollManager mScrollManager;
    private ImageView mThemeSwitcherView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chart);
        mThemeSwitcherView = findViewById(R.id.theme_switcher);

        setUpScrolling();
        setUpContent();
        setUpThemeSwitcher();
        setUpStyleReload();
    }

    @SuppressWarnings("CodeBlock2Expr")
    private void setUpContent() {
        Context context = this;
        ChartDatasHolder dataHolder = new ChartDatasHolder();
        ViewGroup contentHolder = findViewById(R.id.charts);
        for (ChartData chartData : dataHolder.datas) {
            ViewGroup row = (ViewGroup) getLayoutInflater().inflate(R.layout.layout_chart_row, contentHolder, false);

            TextView title = row.findViewById(R.id.title);
            ChartView chart = row.findViewById(R.id.chart);
            NavigatorChartView navigator = row.findViewById(R.id.navigator);
            FlowLayout selectors = row.findViewById(R.id.selectors);

            mStyleables.add(chart);
            mStyleables.add(navigator);

            title.setText(chartData.getName());

            applyConfigs(chart, navigator);
            navigator.apply(LeonardoConfigFactory.asNavigatorShowCase(chart));
            navigator.setScrollListener(new ScrollListener() {
                @Override
                public void onStarted() {
                    mScrollManager.setScrollOwner(navigator);
                }

                @Override
                public void onStopped() {
                    mScrollManager.setScrollOwner(null);
                }
            });

            ChartModel chartModel = new ChartModelImpl(3);

            for (ChartDataSource dataSource : chartData.getDataSources()) {
                chartModel.addDataSource(dataSource);

                DataSourceSelectorView selectorView = new DataSourceSelectorView(this);
                ReloadableStyleable styleReloader =
                        () -> selectorView.setColors(dataSource.getSelectorColor(isLightThemeActive()),
                                                     LeonardoUtil.getColor(context, android.R.attr.windowBackground));
                mStyleables.add(styleReloader);
                styleReloader.reloadStyle();
                selectorView.setDataSourceName(dataSource.getLegend());
                selectorView.setCallback(selected -> {
                    if (selected) {
                        chartModel.enableDataSource(dataSource);
                    } else {
                        chartModel.disableDataSource(dataSource);
                    }
                });

                selectors.addView(selectorView);
            }
            chartModel.setActiveRange(chartData.getDataSources().iterator().next().getDataRange(), navigator);

            chart.apply(chartModel, chartData.getType());
            navigator.apply(chartModel, chartData.getType());

            contentHolder.addView(row);
        }
    }


    private void setUpScrolling() {
        mScrollManager = new ScrollManager();
        MyScrollView scrollView = findViewById(R.id.main_scroll_view);
        scrollView.setScrollManager(mScrollManager);
    }

    private void setUpThemeSwitcher() {
        mThemeSwitcherView.setOnClickListener(v -> {
            if (isLightThemeActive()) {
                setTheme(R.style.AppTheme_Dark);
                mThemeSwitcherView.setImageResource(R.drawable.ic_sun);
            } else {
                setTheme(R.style.AppTheme_Light);
                mThemeSwitcherView.setImageResource(R.drawable.ic_moon);
            }

            for (ReloadableStyleable styleable : mStyleables) {
                styleable.reloadStyle();
            }
        });
    }

    private boolean isLightThemeActive() {
        TypedValue typedValue = new TypedValue();
        getTheme().resolveAttribute(R.attr.theme_name, typedValue, true);
        return "light".equals(typedValue.string.toString());
    }

    @SuppressWarnings("CodeBlock2Expr")
    private void setUpStyleReload() {
        View contentHolder = findViewById(R.id.content);
        mStyleables.add(() -> {
            contentHolder.setBackgroundColor(LeonardoUtil.getColor(this, android.R.attr.windowBackground));
        });

        View actionBar = findViewById(R.id.action_bar);
        mStyleables.add(() -> {
            actionBar.setBackgroundColor(LeonardoUtil.getColor(this, R.attr.action_bar_background_color));
        });
    }

    private void applyConfigs(ChartView chart, NavigatorChartView navigator) {
        Context context = this;

        ChartConfigProvider chartConfigProvider = new ChartConfigProvider() {
            @Nonnull
            @Override
            public ChartConfig buildConfig() {
                AxisConfig xAxisConfig = LeonardoConfigFactory.newAxisConfigBuilder()
                                                              .withLabelTextStrategy(TimeAxisLabelTextStrategy.INSTANCE)
                                                              .withContext(context)
                                                              .build();

                return LeonardoConfigFactory.newChartConfigBuilder()
                                            .withContext(context)
                                            .withXAxisConfig(xAxisConfig)
                                            .withTheme(isLightThemeActive())
                                            .build();
            }
        };

        ChartConfig chartConfig = chartConfigProvider.buildConfig();
        chart.apply(chartConfig);
        chart.setChartConfigProvider(chartConfigProvider);

        NavigatorConfigProvider navigatorConfigProvider = new NavigatorConfigProvider() {
            @Nonnull
            @Override
            public NavigatorConfig buildConfig() {
                return LeonardoConfigFactory.newNavigatorConfigBuilder()
                                            .withContext(context)
                                            .build();
            }
        };
        navigator.apply(navigatorConfigProvider.buildConfig(), chartConfig);
        navigator.setNavigatorConfigProvider(navigatorConfigProvider);
        navigator.setChartConfigProvider(chartConfigProvider);
    }
}
