package harmonysoft.tech.chartexample.view.scroll;

import tech.harmonysoft.android.leonardo.view.navigator.NavigatorChartView;

import javax.annotation.Nullable;

public class ScrollManager {

    private NavigatorChartView mScrollOwner;
    private boolean mSkipStopScroll;

    @Nullable
    public NavigatorChartView getScrollOwner() {
        return mScrollOwner;
    }

    public void setScrollOwner(@Nullable NavigatorChartView scrollOwner) {
        if (!mSkipStopScroll && mScrollOwner != null && mScrollOwner != scrollOwner) {
            mSkipStopScroll = true;
            mScrollOwner.stopAction();
            mSkipStopScroll = false;
        }
        mScrollOwner = scrollOwner;
    }
}
