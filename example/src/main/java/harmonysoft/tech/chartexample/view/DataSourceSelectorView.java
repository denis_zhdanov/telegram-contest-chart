package harmonysoft.tech.chartexample.view;

import android.content.Context;
import android.graphics.*;
import android.util.AttributeSet;
import android.view.View;
import harmonysoft.tech.chartexample.R;
import tech.harmonysoft.android.leonardo.view.util.RoundedRectangleDrawer;

public class DataSourceSelectorView extends View {

    private final Paint mBorderPaint = new Paint();
    private final Paint mFillPaint   = new Paint();
    private final Paint mDrawPaint   = new Paint();
    private final Rect  mBounds      = new Rect();

    private final RoundedRectangleDrawer mRoundedRectangleDrawer = RoundedRectangleDrawer.INSTANCE;

    private Listener mCallback;
    private RectF    mCachedBounds;

    private boolean mChecked = true;

    private String mDataSourceName;
    private int    mDataSourceColor;
    private int    mDataSourceNameWidth;
    private int    mUncheckedBackgroundColor;
    private int    mSignEdge;
    private int    mSpacing;
    private int    mTextHeight;

    public DataSourceSelectorView(Context context) {
        super(context);
        init();
    }

    public DataSourceSelectorView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public DataSourceSelectorView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        mBorderPaint.setStyle(Paint.Style.STROKE);
        mFillPaint.setStyle(Paint.Style.FILL);
        mDrawPaint.setStyle(Paint.Style.FILL);

        int textSize = getResources().getDimensionPixelSize(R.dimen.data_source_selector_text_size);
        mDrawPaint.setTextSize(textSize);
        mBorderPaint.setStrokeWidth(textSize / 7f);
        mDrawPaint.setStrokeWidth(textSize / 7f);
        setOnClickListener(v -> {
            mChecked = !mChecked;
            if (mCallback != null) {
                mCallback.onSelectionChange(mChecked);
            }
            invalidate();
        });
    }

    public void setDataSourceName(String dataSourceName) {
        mDataSourceName = dataSourceName;
    }

    public void setColors(int activeColor, int backgroundColor) {
        mDataSourceColor = activeColor;
        mBorderPaint.setColor(activeColor);
        mUncheckedBackgroundColor = backgroundColor;
        invalidate();
    }

    public void setCallback(Listener callback) {
        mCallback = callback;
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        mDrawPaint.getTextBounds("W", 0, 1, mBounds);
        mTextHeight = mBounds.height();
        int baseSymbolWidth = mBounds.width();
        mSignEdge = mTextHeight;
        mSpacing = baseSymbolWidth;
        int height = (int) (mTextHeight * 3f);
        height += height % 2;
        mDrawPaint.getTextBounds(mDataSourceName, 0, mDataSourceName.length(), mBounds);
        mDataSourceNameWidth = mBounds.width();
        int width = mDataSourceNameWidth + height + mSignEdge + mSpacing;
        setMeasuredDimension(width, height);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        int width = getWidth();
        int height = getHeight();
        if (mCachedBounds == null || mCachedBounds.width() != width || mCachedBounds.height() != height) {
            mCachedBounds = new RectF(0, 0, width, height);
        }

        if (mChecked) {
            mFillPaint.setColor(mDataSourceColor);
            mRoundedRectangleDrawer.draw(mCachedBounds,
                                         mBorderPaint,
                                         mFillPaint,
                                         getHeight() / 2f - mBorderPaint.getStrokeWidth(),
                                         canvas);

            float signLeft = height / 2f;
            float signTop = (height - mSignEdge) / 2f;
            float middleX = signLeft + mSignEdge / 3f;
            float middleY = signTop + mSignEdge;
            mDrawPaint.setColor(Color.WHITE);
            canvas.drawLine(signLeft, signTop + mSignEdge * 14f / 23f, middleX, middleY, mDrawPaint);
            canvas.drawLine(middleX, middleY, signLeft + mSignEdge, signTop, mDrawPaint);

            mDrawPaint.setColor(Color.WHITE);
            canvas.drawText(mDataSourceName,
                            height / 2f + mSignEdge + mSpacing,
                            (getHeight() - mTextHeight) / 2f + mTextHeight,
                            mDrawPaint);
        } else {
            mFillPaint.setColor(mUncheckedBackgroundColor);
            mRoundedRectangleDrawer.draw(mCachedBounds,
                                         mBorderPaint,
                                         mFillPaint,
                                         getHeight() / 2f - mBorderPaint.getStrokeWidth(),
                                         canvas);

            mDrawPaint.setColor(mDataSourceColor);
            canvas.drawText(mDataSourceName,
                            (width - mDataSourceNameWidth) / 2f,
                            (getHeight() - mTextHeight) / 2f + mTextHeight,
                            mDrawPaint);
        }
    }

    public interface Listener {

        void onSelectionChange(boolean selected);
    }
}
