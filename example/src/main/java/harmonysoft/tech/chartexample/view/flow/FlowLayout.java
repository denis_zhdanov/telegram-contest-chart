package harmonysoft.tech.chartexample.view.flow;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import harmonysoft.tech.chartexample.R;
import tech.harmonysoft.android.leonardo.util.LeonardoUtil;

public class FlowLayout extends ViewGroup {

    private int mSpacing;

    public FlowLayout(Context context) {
        super(context);
        init(context);
    }

    public FlowLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public FlowLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    private void init(Context context) {
        mSpacing = LeonardoUtil.getDimensionSizeInPixels(context, R.attr.flow_layout_spacing);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        final int widthLimit = MeasureSpec.getSize(widthMeasureSpec) - getPaddingLeft() - getPaddingRight();

        boolean firstInRow = true;
        int left = getPaddingLeft();
        int rowTop = getPaddingTop();
        int rowHeight = 0;
        int firstRowChildIndex = 0;

        int childCount = getChildCount();
        for (int i = 0; i < childCount; i++) {
            View child = getChildAt(i);
            measureChild(child, widthMeasureSpec, heightMeasureSpec);

            if (firstInRow) {
                firstInRow = false;
            } else {
                left += mSpacing;
            }

            if (left + child.getMeasuredWidth() > widthLimit) {
                setTop(rowTop, rowHeight, firstRowChildIndex, i);
                firstInRow = true;
                left = getPaddingLeft();
                rowTop += rowHeight + mSpacing;
                rowHeight = 0;
                firstRowChildIndex = i;
            }

            ((LayoutParams) child.getLayoutParams()).setLeft(left);
            left += child.getMeasuredWidth();
            rowHeight = Math.max(rowHeight, child.getMeasuredHeight());
        }

        if (firstRowChildIndex < childCount) {
            setTop(rowTop, rowHeight, firstRowChildIndex, childCount);
            rowTop += rowHeight;
        }

        setMeasuredDimension(widthLimit, rowTop + getPaddingBottom());
    }

    private void setTop(int rowTop, int rowHeight, int indexFrom, int indexTo) {
        for (int i = indexFrom; i < indexTo; i++) {
            View child = getChildAt(i);
            LayoutParams params = (LayoutParams) child.getLayoutParams();
            params.setTop(rowTop + (rowHeight - child.getMeasuredHeight()) / 2);
        }
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        int childCount = getChildCount();
        for (int i = 0; i < childCount; i++) {
            View child = getChildAt(i);
            LayoutParams layoutParams = (LayoutParams) child.getLayoutParams();
            child.layout(layoutParams.getLeft(),
                         layoutParams.getTop(),
                         layoutParams.getLeft() + child.getMeasuredWidth(),
                         layoutParams.getTop() + child.getMeasuredHeight());
        }
    }

    @Override
    protected ViewGroup.LayoutParams generateDefaultLayoutParams() {
        return new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
    }

    public static class LayoutParams extends ViewGroup.LayoutParams {

        private int mLeft;
        private int mTop;

        public LayoutParams(Context c, AttributeSet attrs) {
            super(c, attrs);
        }

        public LayoutParams(int width, int height) {
            super(width, height);
        }

        public LayoutParams(ViewGroup.LayoutParams source) {
            super(source);
        }

        public int getLeft() {
            return mLeft;
        }

        public void setLeft(int left) {
            mLeft = left;
        }

        public int getTop() {
            return mTop;
        }

        public void setTop(int top) {
            mTop = top;
        }
    }
}
