package harmonysoft.tech.chartexample.view;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Point;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewParent;
import android.widget.ScrollView;
import harmonysoft.tech.chartexample.view.scroll.ScrollManager;
import tech.harmonysoft.android.leonardo.view.navigator.NavigatorChartView;

import javax.annotation.Nonnull;
import java.util.HashMap;
import java.util.Map;

public class MyScrollView extends ScrollView {

    private final Map<View, Point> mLocationCache = new HashMap<>();

    private ScrollManager mScrollManager;

    public MyScrollView(Context context) {
        super(context);
    }

    public MyScrollView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public MyScrollView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void setScrollManager(ScrollManager scrollManager) {
        mScrollManager = scrollManager;
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public boolean onTouchEvent(MotionEvent e) {
        if (mScrollManager == null) {
            return super.onTouchEvent(e);
        }

        NavigatorChartView scrollOwner = mScrollManager.getScrollOwner();
        if (scrollOwner == null) {
            return super.onTouchEvent(e);
        }

        Point location = getRelativeY(scrollOwner);
        int action = e.getActionMasked();
        if (action == MotionEvent.ACTION_MOVE) {
            scrollOwner.move(e.getX() - location.x);
        } else {
            mScrollManager.setScrollOwner(null);
        }
        return true;
    }

    @Nonnull
    private Point getRelativeY(View view) {
        Point cached = mLocationCache.get(view);
        if (cached != null) {
            return cached;
        }

        int x = 0;
        int y = 0;
        View v = view;
        while (v != this) {
            x += v.getLeft();
            y += v.getTop();
            ViewParent parent = v.getParent();
            if (parent instanceof View) {
                v = (View) parent;
            } else {
                break;
            }
        }

        Point result = new Point(x, y);
        mLocationCache.put(view, result);
        return result;
    }
}
