package harmonysoft.tech.chartexample.data;

import tech.harmonysoft.android.leonardo.model.DataPoint;
import tech.harmonysoft.android.leonardo.model.Range;
import tech.harmonysoft.android.leonardo.model.data.ChartDataLoader;
import tech.harmonysoft.android.leonardo.util.DataPointAnchor;
import tech.harmonysoft.android.leonardo.util.WithComparableLongProperty;

import javax.annotation.Nullable;
import java.util.Collection;
import java.util.NavigableSet;
import java.util.TreeSet;

/**
 * @author Denis Zhdanov
 * @since 20/3/19
 */
public class PreDefinedChartDataLoader implements ChartDataLoader {

    private final NavigableSet<WithComparableLongProperty> mPoints = new TreeSet<>(WithComparableLongProperty.COMPARATOR);

    private final DataPointAnchor mBottomDataAnchor = new DataPointAnchor();
    private final DataPointAnchor mTopDataAnchor = new DataPointAnchor();

    public PreDefinedChartDataLoader(Collection<DataPoint> points) {
        mPoints.addAll(points);
    }

    @SuppressWarnings("unchecked")
    @Nullable
    @Override
    public Collection<DataPoint> load(Range range) {
        if (mPoints.isEmpty()
            || range.getStart() > mPoints.last().getProperty()
            || range.getEnd() < mPoints.first().getProperty())
        {
            return null;
        }

        mTopDataAnchor.setValue(range.getEnd());
        NavigableSet<WithComparableLongProperty> filteredFromTop = mPoints.headSet(mTopDataAnchor, true);

        mBottomDataAnchor.setValue(range.getStart());
        return (NavigableSet)filteredFromTop.tailSet(mBottomDataAnchor, true);
    }
}
