package harmonysoft.tech.chartexample.data;

import tech.harmonysoft.android.leonardo.model.data.ChartData;
import java.util.List;
import java.util.ArrayList;

public class ChartDatasHolder {

    public final List<ChartData> datas = new ArrayList<>();

    public ChartDatasHolder() {
        datas.add(new ChartDataHolder0().data);
        datas.add(new ChartDataHolder1().data);
        datas.add(new ChartDataHolder2().data);
        datas.add(new ChartDataHolder3().data);
        datas.add(new ChartDataHolder4().data);
    }
}