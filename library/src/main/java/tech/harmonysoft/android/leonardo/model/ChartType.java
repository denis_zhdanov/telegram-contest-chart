package tech.harmonysoft.android.leonardo.model;

public enum ChartType {
    LINE, LINE_2_Y, STACKED_BAR, PERCENTAGE_STACKED, PIE
}
