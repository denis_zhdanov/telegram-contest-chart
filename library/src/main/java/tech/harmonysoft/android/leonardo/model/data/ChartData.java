package tech.harmonysoft.android.leonardo.model.data;

import tech.harmonysoft.android.leonardo.model.ChartType;

import javax.annotation.Nonnull;
import java.util.ArrayList;
import java.util.Collection;

public class ChartData {

    private final Collection<ChartDataSource> mDataSources = new ArrayList<>();

    private final String mName;
    private final ChartType mType;

    public ChartData(String name, ChartType type, Collection<ChartDataSource> dataSources) {
        mName = name;
        mType = type;
        mDataSources.addAll(dataSources);
    }

    @Nonnull
    public Collection<ChartDataSource> getDataSources() {
        return mDataSources;
    }

    @Nonnull
    public String getName() {
        return mName;
    }

    @Nonnull
    public ChartType getType() {
        return mType;
    }

    @Nonnull
    @Override
    public String toString() {
        return mName;
    }
}
