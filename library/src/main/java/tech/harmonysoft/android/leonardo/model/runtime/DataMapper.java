package tech.harmonysoft.android.leonardo.model.runtime;

import tech.harmonysoft.android.leonardo.model.DataPoint;
import tech.harmonysoft.android.leonardo.model.VisualPoint;

import javax.annotation.Nonnull;

public interface DataMapper {

    float dataXToVisualX(long dataX);

    long visualXToDataX(float visualX);

    @Nonnull
    VisualPoint dataPointToVisualPoint(DataPoint dataPoint);
}
