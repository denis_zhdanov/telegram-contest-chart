package tech.harmonysoft.android.leonardo.view.chart;

import tech.harmonysoft.android.leonardo.model.data.ChartDataSource;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.HashMap;
import java.util.Map;

public class BarSlice {

    private final Map<ChartDataSource, Long> mDataYs = new HashMap<>();

    private final long mDataX;
    private final long mXLength;

    public BarSlice(Map<ChartDataSource, Long> dataYs, long dataX, long XLength) {
        mDataYs.putAll(dataYs);
        mDataX = dataX;
        mXLength = XLength;
    }

    @Nonnull
    public Map<ChartDataSource, Long> getDataYs() {
        return mDataYs;
    }

    @Nullable
    public Long getDataY(ChartDataSource dataSource) {
        return mDataYs.get(dataSource);
    }

    public long getDataX() {
        return mDataX;
    }

    public long getXLength() {
        return mXLength;
    }
}
