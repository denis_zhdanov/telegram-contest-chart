package tech.harmonysoft.android.leonardo.view.chart;

import android.animation.ValueAnimator;
import android.view.View;
import android.view.animation.LinearInterpolator;
import tech.harmonysoft.android.leonardo.model.data.ChartDataSource;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class PlotAnimator {

    private final Map<ChartDataSource, DataSourceAnimationContext> mAnimationDataSourceInfo = new HashMap<>();

    private final ValueAnimator mAnimator = new ValueAnimator();

    private final View mView;
    private final long mAnimationDurationMillis;

    public PlotAnimator(View view, long animationDurationMillis) {
        mView = view;
        mAnimationDurationMillis = animationDurationMillis;

        mAnimator.setInterpolator(new LinearInterpolator());
        mAnimator.setDuration(animationDurationMillis);
        mAnimator.addUpdateListener(animation -> tickAnimation());
    }

    private void tickAnimation() {
        Set<ChartDataSource> toRemove = new HashSet<>();
        long now = System.currentTimeMillis();
        for (Map.Entry<ChartDataSource, DataSourceAnimationContext> entry : mAnimationDataSourceInfo.entrySet()) {
            if (now >= entry.getValue().startTimeMs + mAnimationDurationMillis) {
                toRemove.add(entry.getKey());
                break;
            }

            long elapsedTimeMs = now - entry.getValue().startTimeMs;
            int totalAlphaDelta = entry.getValue().finalAlpha - entry.getValue().initialAlpha;
            int currentAlphaDelta = (int) (elapsedTimeMs * totalAlphaDelta / mAnimationDurationMillis);
            entry.getValue().currentAlpha = entry.getValue().initialAlpha + currentAlphaDelta;
            if ((totalAlphaDelta > 0 && entry.getValue().currentAlpha >= entry.getValue().finalAlpha)
                || (totalAlphaDelta < 0 && entry.getValue().currentAlpha <= entry.getValue().finalAlpha))
            {
                toRemove.add(entry.getKey());
            }
        }

        if (!toRemove.isEmpty()) {
            for (ChartDataSource dataSource : toRemove) {
                mAnimationDataSourceInfo.remove(dataSource);
            }
        }

        if (!mAnimationDataSourceInfo.isEmpty() || !toRemove.isEmpty()) {
            mView.invalidate();
        }
    }

    public void fadeIn(ChartDataSource dataSource) {
        mAnimationDataSourceInfo.put(dataSource,new DataSourceAnimationContext(0, 255));
        startAnimation();
    }

    public void fadeOut(ChartDataSource dataSource) {
        mAnimationDataSourceInfo.put(dataSource,new DataSourceAnimationContext(255, 0));
        startAnimation();
    }

    private void startAnimation() {
        mAnimator.cancel();
        mAnimator.setIntValues(0, 255);
        mAnimator.start();
    }

    public boolean isAnimationInProgress(ChartDataSource dataSource) {
        return mAnimationDataSourceInfo.containsKey(dataSource);
    }

    public int getAlpha(ChartDataSource dataSource) {
        DataSourceAnimationContext context = mAnimationDataSourceInfo.get(dataSource);
        return context == null ? 255 : context.currentAlpha;
    }

    private static class DataSourceAnimationContext {

        public final int initialAlpha;
        public final int finalAlpha;
        public final long startTimeMs;

        public int currentAlpha;

        public DataSourceAnimationContext(int initialAlpha, int finalAlpha) {
            this.initialAlpha = initialAlpha;
            this.finalAlpha = finalAlpha;
            startTimeMs = System.currentTimeMillis();
            currentAlpha = initialAlpha;
        }
    }
}
