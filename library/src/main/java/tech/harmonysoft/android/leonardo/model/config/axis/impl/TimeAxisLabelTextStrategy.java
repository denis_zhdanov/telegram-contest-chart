package tech.harmonysoft.android.leonardo.model.config.axis.impl;

import android.annotation.SuppressLint;
import tech.harmonysoft.android.leonardo.model.axis.AxisLabelTextStrategy;
import tech.harmonysoft.android.leonardo.model.axis.LabelType;
import tech.harmonysoft.android.leonardo.model.text.TextWrapper;

import javax.annotation.Nonnull;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

/**
 * @author Denis Zhdanov
 * @since 21/3/19
 */
@SuppressLint("SimpleDateFormat")
public class TimeAxisLabelTextStrategy implements AxisLabelTextStrategy {

    public static final TimeAxisLabelTextStrategy INSTANCE = new TimeAxisLabelTextStrategy();

    public static final long MILLISECOND = 1;
    public static final long SECOND      = TimeUnit.SECONDS.toMillis(1);
    public static final long MINUTE      = TimeUnit.MINUTES.toMillis(1);
    public static final long HOUR        = TimeUnit.HOURS.toMillis(1);
    public static final long DAY         = TimeUnit.DAYS.toMillis(1);
    public static final long MONTH       = DAY * 30;
    public static final long YEAR        = 365 * DAY;
    public static final long LEAP_YEAR   = YEAR + DAY + 30 * MINUTE;
    public static final long TWO_YEARS   = 2 * YEAR;
    public static final long YEAR_CYCLE  = 4 * YEAR + DAY;

    private static final DayInfo[] MONTHS = new DayInfo[365];

    static {
        int offset = 0;
        offset += addMonths("Jan", offset, 31);
        offset += addMonths("Feb", offset, 28);
        offset += addMonths("Mar", offset, 31);
        offset += addMonths("Apr", offset, 30);
        offset += addMonths("May", offset, 31);
        offset += addMonths("Jun", offset, 30);
        offset += addMonths("Jul", offset, 31);
        offset += addMonths("Aug", offset, 31);
        offset += addMonths("Sep", offset, 30);
        offset += addMonths("Oct", offset, 31);
        offset += addMonths("Nov", offset, 30);
        addMonths("Dec", offset, 31);
    }

    private static int addMonths(String monthName, int offset, int length) {
        for (int i = 0; i < length; i++) {
            MONTHS[offset + i] = new DayInfo(monthName, i + 1);
        }
        return length;
    }

    private static final long FEBRUARY_29 = 31 + 28 - 1;

    private static final char[][] DAYS_OF_WEEK_SMALL = {
            "Thu".toCharArray(), "Fri".toCharArray(), "Sat".toCharArray(), "Sun".toCharArray(), "Mon".toCharArray(),
            "Tue".toCharArray(), "Wed".toCharArray()
    };

    private static final char[][] DAYS_OF_WEEK_FULL = {
            "Thursday".toCharArray(), "Friday".toCharArray(), "Saturday".toCharArray(), "Sunday".toCharArray(),
            "Monday".toCharArray(), "Tuesday".toCharArray(), "Wednesday".toCharArray()
    };

    private static final Formatter FORMATTER_MILLISECOND = (value, text, type) -> {
        long trimmed = value % MINUTE;
        appendAndPadIfNecessary(trimmed / SECOND, 10, text);
        text.append('.');
        appendAndPadIfNecessary(trimmed % SECOND, 100, text);
    };

    private static final Formatter FORMATTER_SECOND = (value, text, type) -> {
        long trimmed = value % HOUR;
        appendAndPadIfNecessary(trimmed / MINUTE, 10, text);
        text.append(':');
        appendAndPadIfNecessary((trimmed % MINUTE) / SECOND, 10, text);
    };

    private static final Formatter FORMATTER_MINUTE = (value, text, type) -> {
        long trimmed = value % DAY;
        appendAndPadIfNecessary(trimmed / HOUR, 10, text);
        text.append(':');
        appendAndPadIfNecessary((trimmed % HOUR) / MINUTE, 10, text);
    };

    private static final Formatter FORMATTER_DAY =
            (value, text, type) -> processLeapAwareTime(value, text, type, false, true, true);

    private static final Formatter FORMATTER_MONTH =
            (value, text, type) -> processLeapAwareTime(value, text, type, false, true, false);

    private static final Formatter FORMATTER_YEAR =
            (value, text, type) -> processLeapAwareTime(value, text, type, true, false, false);

    private static void processLeapAwareTime(long value,
                                             TextWrapper text,
                                             LabelType type,
                                             boolean printYear,
                                             boolean printMonth,
                                             boolean printDay)
    {
        int year = 1968;
        long trimmed = (value + TWO_YEARS) % YEAR_CYCLE;
        year += 4 * ((value + TWO_YEARS) / YEAR_CYCLE);
        boolean leap = trimmed <= LEAP_YEAR;
        while (trimmed >= YEAR) {
            trimmed -= YEAR;
            year++;
        }

        long days = trimmed / DAY;

        if (type == LabelType.TITLE_SMALL) {
            text.append(DAYS_OF_WEEK_SMALL[(int) ((value / DAY) % DAYS_OF_WEEK_SMALL.length)]);
            text.append(", ");
            if (leap && days == FEBRUARY_29) {
                text.append("29 Feb ");
            } else {
                DayInfo dayInfo = MONTHS[(int) days];
                text.append(dayInfo.day);
                text.append(' ');
                text.append(dayInfo.monthShort);
                text.append(' ');
            }
            text.append(year);
            return;
        }

        if (type == LabelType.TITLE_FULL) {
            text.append(DAYS_OF_WEEK_FULL[(int) ((value / DAY) % DAYS_OF_WEEK_SMALL.length)]);
            text.append(", ");
            if (leap && days == FEBRUARY_29) {
                text.append("29 February");
            } else {
                DayInfo dayInfo = MONTHS[(int) days];
                text.append(dayInfo.day);
                text.append(' ');
                text.append(dayInfo.monthLong);
            }
            text.append(year);
            return;
        }

        if (printYear) {
            text.append(year);
            if (!printMonth) {
                return;
            }
        }

        if (leap && days == FEBRUARY_29) {
            text.append(MONTHS[32].monthShort);
            if (printDay) {
                text.append(" 29");
            }

        } else {
            DayInfo dayInfo = MONTHS[(int) days];
            text.append(dayInfo.monthShort);
            if (printDay) {
                text.append(' ');
                text.append(dayInfo.day);
            }
        }
    }

    private static void appendAndPadIfNecessary(long value, long base, TextWrapper text) {
        while (base > 0) {
            text.append(value / base);
            value %= base;
            base /= 10;
        }
    }

    private final TextWrapper mText = new TextWrapper();

    private final long mOffset;

    public TimeAxisLabelTextStrategy() {
        this(TimeZone.getDefault());
    }

    public TimeAxisLabelTextStrategy(TimeZone timeZone) {
        mOffset = timeZone.getRawOffset();
    }

    @Nonnull
    @Override
    public TextWrapper getLabel(final long value, long step, LabelType type) {
        mText.reset();
        final Formatter formatter;
        if (step >= MILLISECOND && step < SECOND) {
            formatter = FORMATTER_MILLISECOND;
        } else if (step >= SECOND && step < MINUTE) {
            formatter = FORMATTER_SECOND;
        } else if (step >= MINUTE && step < DAY) {
            formatter = FORMATTER_MINUTE;
        } else if (step >= DAY && step < MONTH) {
            formatter = FORMATTER_DAY;
        } else if (step >= MONTH && step < YEAR) {
            formatter = FORMATTER_MONTH;
        } else {
            formatter = FORMATTER_YEAR;
        }
        formatter.format(value + mOffset, mText, type);
        return mText;
    }

    private interface Formatter {

        void format(long value, TextWrapper text, LabelType type);
    }

    private static class DayInfo {

        public final char[] monthShort;
        public final char[] monthLong;
        public final int    day;

        public DayInfo(String monthName, int day) {
            this.monthShort = monthName.substring(0, 3).toCharArray();
            this.monthLong = monthName.toCharArray();
            this.day = day;
        }
    }
}
