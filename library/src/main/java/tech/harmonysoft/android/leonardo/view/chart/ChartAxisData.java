package tech.harmonysoft.android.leonardo.view.chart;

import android.graphics.Paint;
import android.graphics.Rect;
import tech.harmonysoft.android.leonardo.model.Range;
import tech.harmonysoft.android.leonardo.model.axis.AxisLabelTextStrategy;
import tech.harmonysoft.android.leonardo.model.config.axis.AxisConfig;

public class ChartAxisData {

    public final AxisAnimator          animator;
    public final AxisLabelTextStrategy labelTextStrategy;
    public final int                   labelWidth;
    public final int                   labelHeight;
    public final int                   labelPadding;
    public final boolean               drawAxis;

    public Range range = Range.EMPTY_RANGE;

    public long  axisStep;
    public float unitSize;
    public float visualShift;
    public int   availableSize;

    public ChartAxisData(Paint labelPaint, AxisConfig axisConfig, AxisAnimator animator) {
        this.animator = animator;
        labelTextStrategy = axisConfig.getLabelTextStrategy();
        Rect bounds = new Rect();
        labelPaint.getTextBounds("W", 0, 1, bounds);
        labelWidth = bounds.width();
        labelHeight = bounds.height();
        labelPadding = labelHeight / 2;
        drawAxis = axisConfig.shouldDrawAxis();
    }

    public float dataValueToVisualValue(long dataValue) {
        return visualShift + (dataValue - range.getStart()) * ((float) availableSize) / range.getSize();
    }

    public long visualValueToDataValue(float visualValue) {
        return range.getStart() + Math.round((visualValue - visualShift) * range.getSize() / (double) availableSize);
    }
}
