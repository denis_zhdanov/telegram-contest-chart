package tech.harmonysoft.android.leonardo.util;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Collection;
import java.util.Iterator;

public class IterableEx<T> implements Iterable<T> {

    private Collection<T> mDelegate;
    private T mFirst;
    private T mLast;
    private T mFirstInCollection;
    private T mLastInCollection;

    public void refresh(Collection<T> delegate, @Nullable T first, @Nullable T last) {
        mDelegate = delegate;
        mFirst = first;
        mLast = last;
        mFirstInCollection = null;
        mLastInCollection = null;
    }

    public boolean isFirst(T t) {
        if (mFirst != null) {
            return mFirst == t;
        }
        if (!mDelegate.isEmpty()) {
            return mFirstInCollection == t;
        }
        return t == mLast;
    }

    public boolean isLast(T t) {
        if (mLast != null) {
            return mLast == t;
        }
        if (!mDelegate.isEmpty()) {
            return mLastInCollection == t;
        }
        return mFirst == t;
    }

    @Nonnull
    @Override
    public Iterator<T> iterator() {
        return new Iterator<T>() {

            private final Iterator<T> mIterator = mDelegate.iterator();
            private boolean mFirstChecked = false;
            private boolean mLastChecked = false;

            @Override
            public boolean hasNext() {
                return (!mFirstChecked && mFirst != null) || mIterator.hasNext() || (!mLastChecked && mLast != null);
            }

            @Override
            public T next() {
                if (!mFirstChecked) {
                    mFirstChecked = true;
                    if (mFirst != null) {
                        return mFirst;
                    }
                }
                if (mIterator.hasNext()) {
                    T result = mIterator.next();
                    if (mFirstInCollection == null) {
                        mFirstInCollection = result;
                    }
                    if (!mIterator.hasNext()) {
                        mLastInCollection = result;
                    }
                    return result;
                }
                if (!mLastChecked) {
                    mLastChecked = true;
                    if (mLast != null) {
                        return mLast;
                    }
                }
                return null;
            }
        };
    }
}
