package tech.harmonysoft.android.leonardo.view.chart;

import android.graphics.Canvas;
import tech.harmonysoft.android.leonardo.model.ChartType;
import tech.harmonysoft.android.leonardo.model.config.chart.ChartConfig;
import tech.harmonysoft.android.leonardo.model.runtime.ChartModel;
import tech.harmonysoft.android.leonardo.view.chart.legend.LegendDrawContext;

public interface DataPointsDrawer {

    void draw(Canvas canvas,
              ChartModel model,
              ChartDrawData drawData,
              ChartPalette palette,
              ChartType chartType,
              ChartConfig config,
              Object dataAnchor);

    void initLegendLocation(LegendDrawContext context, ChartDrawData drawData, ChartModel model);

    void drawSelectionOverlay(ChartDrawData drawData,
                              ChartModel model,
                              Canvas canvas,
                              ChartConfig config,
                              ChartPalette palette);
}
