package tech.harmonysoft.android.leonardo.view.navigator;

public interface ScrollListener {

    void onStarted();

    void onStopped();
}
