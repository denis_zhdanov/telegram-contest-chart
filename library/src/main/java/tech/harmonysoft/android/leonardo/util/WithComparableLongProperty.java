package tech.harmonysoft.android.leonardo.util;

import java.util.Comparator;

public interface WithComparableLongProperty {

    Comparator<WithComparableLongProperty> COMPARATOR = (o1, o2) -> Long.compare(o1.getProperty(), o2.getProperty());

    long getProperty();
}
