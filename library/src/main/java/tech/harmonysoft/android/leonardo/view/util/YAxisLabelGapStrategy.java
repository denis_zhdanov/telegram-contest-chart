package tech.harmonysoft.android.leonardo.view.util;

/**
 * @author Denis Zhdanov
 * @since 14/3/19
 */
public class YAxisLabelGapStrategy implements AxisStepChooser.MinGapStrategy {

    public static final YAxisLabelGapStrategy INSTANCE = new YAxisLabelGapStrategy();

    @Override
    public int calculateMinGap(int labelHeight) {
        return labelHeight * 5;
    }
}
