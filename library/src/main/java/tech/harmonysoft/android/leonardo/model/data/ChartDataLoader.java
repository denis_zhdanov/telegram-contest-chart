package tech.harmonysoft.android.leonardo.model.data;

import tech.harmonysoft.android.leonardo.model.DataPoint;
import tech.harmonysoft.android.leonardo.model.Range;

import javax.annotation.Nullable;
import java.util.Collection;

/**
 * @author Denis Zhdanov
 * @since 11/3/19
 */
public interface ChartDataLoader {

    /**
     * Loads target data. Is assumed to be called from a non-main thread.
     *
     * @param range     target X range
     * @return          an interval for the target X range if it's within the current dataset's range;
     *                  {@code null} otherwise
     */
    @Nullable
    Collection<DataPoint> load(Range range);
}
