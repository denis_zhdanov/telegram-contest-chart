package tech.harmonysoft.android.leonardo.view.chart;

import android.graphics.Canvas;
import android.graphics.Paint;
import tech.harmonysoft.android.leonardo.model.ChartType;
import tech.harmonysoft.android.leonardo.model.Range;
import tech.harmonysoft.android.leonardo.model.config.chart.ChartConfig;
import tech.harmonysoft.android.leonardo.model.data.ChartDataSource;
import tech.harmonysoft.android.leonardo.model.runtime.ChartModel;
import tech.harmonysoft.android.leonardo.view.chart.legend.LegendDrawContext;

import java.util.List;
import java.util.NavigableMap;

public class PercentageBarChartDrawer implements DataPointsDrawer {

    public static final PercentageBarChartDrawer INSTANCE = new PercentageBarChartDrawer();

    @Override
    public void draw(Canvas canvas,
                     ChartModel model,
                     ChartDrawData drawData,
                     ChartPalette palette,
                     ChartType chartType,
                     ChartConfig config,
                     Object dataAnchor)
    {
        Paint paint = palette.getPlotPaint();
        paint.setStyle(Paint.Style.FILL);

        List<ChartDataSource> ordered = drawData.getBarDataSources();

        NavigableMap<Long, BarSlice> slices = drawData.getSlices();
        Range xRange = drawData.xAxis.range;
        Long leftDataX = slices.floorKey(xRange.getStart());
        if (leftDataX == null) {
            leftDataX = slices.firstKey();
        }
        if (leftDataX == null) {
            return;
        }
        float leftVisualX = drawData.getChartLeft();
        final float bottomVisualY = drawData.getChartBottom();
        float sliceVisualWidth = 0;

        BarSlice slice = slices.get(leftDataX);
        while (slice == null && leftDataX < xRange.getEnd()) {
            leftDataX = slices.higherKey(leftDataX);
            slice = slices.get(leftDataX);
        }

        while (slice != null && leftVisualX < drawData.getChartRight()) {
            if (sliceVisualWidth <= 0) {
                sliceVisualWidth = drawData.dataXToVisualX(drawData.xAxis.range.getStart() + slice.getXLength())
                                   - drawData.getChartLeft();
            }
            float rightVisualX = drawData.dataXToVisualX(slice.getDataX() + slice.getXLength());

            float total = 0;
            for (ChartDataSource dataSource : ordered) {
                if (!model.isActive(dataSource)) {
                    continue;
                }

                Long dataY = slice.getDataY(dataSource);
                if (dataY == null) {
                    continue;
                }
                total += dataY;
            }

            float bottom = bottomVisualY;
            for (ChartDataSource dataSource : ordered) {
                if (!model.isActive(dataSource)) {
                    continue;
                }

                Long dataY = slice.getDataY(dataSource);
                if (dataY == null) {
                    continue;
                }

                paint.setColor(dataSource.getPlotColor(config.isLight()));
                float percentage = ((float)dataY) / total;
                float visualHeight = percentage * bottomVisualY;
                canvas.drawRect(leftVisualX, bottom - visualHeight, rightVisualX, bottom, paint);
                bottom -= visualHeight;
            }

            leftVisualX = rightVisualX;
            leftDataX = slices.higherKey(leftDataX);
            if (leftDataX == null) {
                break;
            }
            slice = slices.get(leftDataX);
        }
    }

    @Override
    public void initLegendLocation(LegendDrawContext context, ChartDrawData drawData, ChartModel model) {
        context.topOnChart = (drawData.getChartBottom() - context.totalHeight) / 2f;

        float selectionVisualX = drawData.dataXToVisualX(model.getSelectedX());

        if (selectionVisualX >= context.totalWidth + context.horizontalPadding) {
            // We can locate the legend to the left of the selected slice
            context.leftOnChart = Math.max(0, selectionVisualX - context.totalWidth - context.horizontalPadding);
            return;
        }

        if (selectionVisualX + context.horizontalPadding + context.totalWidth <= drawData.getChartRight()) {
            // We can locate the legend to the right of the selected slice
            context.leftOnChart = selectionVisualX + context.horizontalPadding;
            return;
        }

        // The legend doesn't fit neither left nor right of the selection, let's draw it above
        int chartWidth = drawData.getChartRight() - drawData.getChartLeft();
        context.leftOnChart = drawData.getChartLeft() + (chartWidth - context.totalWidth) / 2f;
    }

    @Override
    public void drawSelectionOverlay(ChartDrawData drawData,
                                     ChartModel model,
                                     Canvas canvas,
                                     ChartConfig config,
                                     ChartPalette palette)
    {
        float selectionVisualX = drawData.dataXToVisualX(model.getSelectedX());
        Paint paint = palette.getGridPaint();
        canvas.drawLine(selectionVisualX, 0, selectionVisualX, drawData.getChartBottom(), paint);
    }
}
