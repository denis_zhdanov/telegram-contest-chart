package tech.harmonysoft.android.leonardo.model.axis;

public enum LabelType {
    NORMAL, MINIFIED, TITLE_SMALL, TITLE_FULL
}
