package tech.harmonysoft.android.leonardo.model.config.navigator.impl;

import tech.harmonysoft.android.leonardo.model.config.navigator.NavigatorConfig;

import javax.annotation.Nonnull;

/**
 * @author Denis Zhdanov
 * @since 16/3/19
 */
public class NavigatorConfigImpl implements NavigatorConfig {

    private final int mInactiveChartBackgroundColor;
    private final int mActiveBorderColor;
    private final int mActiveBorderHorizontalWidthInPixels;
    private final int mActiveBorderVerticalHeightInPixels;

    public NavigatorConfigImpl(int inactiveChartBackgroundColor,
                               int activeBorderColor,
                               int activeBorderHorizontalWidthInPixels,
                               int activeBorderVerticalHeightInPixels)
    {
        mInactiveChartBackgroundColor = inactiveChartBackgroundColor;
        mActiveBorderColor = activeBorderColor;
        mActiveBorderHorizontalWidthInPixels = activeBorderHorizontalWidthInPixels;
        mActiveBorderVerticalHeightInPixels = activeBorderVerticalHeightInPixels;
    }

    @Override
    public int getInactiveChartBackgroundColor() {
        return mInactiveChartBackgroundColor;
    }

    @Override
    public int getActiveBorderColor() {
        return mActiveBorderColor;
    }

    @Override
    public int getActiveBorderHorizontalWidthInPixels() {
        return mActiveBorderHorizontalWidthInPixels;
    }

    @Override
    public int getActiveBorderVerticalHeightInPixels() {
        return mActiveBorderVerticalHeightInPixels;
    }

    @Nonnull
    @Override
    public String toString() {
        return "inactiveChartBackgroundColor = " + mInactiveChartBackgroundColor
               + ", activeBorderColor = " + mActiveBorderColor
               + ", activeBorderHorizontalWidthInPixels = " + mActiveBorderHorizontalWidthInPixels
               + ", activeBorderVerticalHeightInPixels = " + mActiveBorderVerticalHeightInPixels;
    }
}
