package tech.harmonysoft.android.leonardo.controller;

import android.annotation.SuppressLint;
import android.os.AsyncTask;
import tech.harmonysoft.android.leonardo.model.DataPoint;
import tech.harmonysoft.android.leonardo.model.Range;
import tech.harmonysoft.android.leonardo.model.data.ChartDataSource;
import tech.harmonysoft.android.leonardo.model.runtime.ChartModel;
import tech.harmonysoft.android.leonardo.model.runtime.ChartModelListener;
import tech.harmonysoft.android.leonardo.util.RangesList;

import javax.annotation.Nonnull;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import static java.util.Collections.emptyList;

/**
 * @author Denis Zhdanov
 * @since 14/3/19
 */
public class ChartDataAutoLoader implements ChartModelListener {

    private final Set<ChartDataLoadTask> mTasks = new HashSet<>();

    private final ChartModel mModel;

    private Range mLastKnownBufferRange = Range.EMPTY_RANGE;

    public ChartDataAutoLoader(ChartModel model) {
        mModel = model;
        model.addListener(this);
    }

    @Override
    public void onRangeChanged(Object anchor) {
        mayBeLoadRanges();
    }

    private void mayBeLoadRanges() {
        Range bufferRange = mModel.getBufferRange();

        if (bufferRange.isEmpty() || (bufferRange.getStart() >= mLastKnownBufferRange.getStart()
                                      && bufferRange.getEnd() <= mLastKnownBufferRange.getEnd()))
        {
            return;
        }

        mLastKnownBufferRange = bufferRange;

        for (ChartDataLoadTask task : mTasks) {
            task.cancel(true);
        }
        mTasks.clear();

        for (ChartDataSource dataSource : mModel.getRegisteredDataSources()) {
            RangesList loadedRanges = mModel.getLoadedRanges(dataSource);

            Collection<Range> rangesToLoad = loadedRanges.getMissing(bufferRange);

            for (Range rangeToLoad : rangesToLoad) {
                ChartDataLoadTask task = new ChartDataLoadTask();
                mTasks.add(task);
                task.execute(new LoadRequest(dataSource, rangeToLoad));
            }
        }
    }

    @Override
    public void onDataSourceEnabled(ChartDataSource dataSource) {
    }

    @Override
    public void onDataSourceDisabled(ChartDataSource dataSource) {
    }

    @Override
    public void onDataSourceAdded(ChartDataSource dataSource) {
        mayBeLoadRanges();
    }

    @Override
    public void onDataSourceRemoved(ChartDataSource dataSource) {
    }

    @Override
    public void onActiveDataPointsLoaded(Object anchor) {
    }

    @Override
    public void onSelectionChange() {
    }

    @SuppressLint("StaticFieldLeak")
    private class ChartDataLoadTask extends AsyncTask<LoadRequest, Void, LoadResult> {

        @Override
        protected LoadResult doInBackground(LoadRequest... requests) {
            if (requests.length != 1) {
                throw new IllegalArgumentException(String.format("Expected to get a single load request but got %d",
                                                                 requests.length));
            }
            LoadRequest request = requests[0];
            Collection<DataPoint> points = request.getDataSource().load(request.getRange());
            return new LoadResult(points == null ? emptyList() : points,
                                  request.getRange(),
                                  request.getDataSource());
        }

        @Override
        protected void onPostExecute(LoadResult loadResult) {
            mTasks.remove(this);
            mModel.onPointsLoaded(loadResult.source, loadResult.range, loadResult.points);
        }
    }

    private static final class LoadRequest {

        private final ChartDataSource mDataSource;
        private final Range           mRange;

        public LoadRequest(ChartDataSource dataSource, Range range) {
            mDataSource = dataSource;
            mRange = range;
        }

        @Nonnull
        public ChartDataSource getDataSource() {
            return mDataSource;
        }

        @Nonnull
        public Range getRange() {
            return mRange;
        }
    }

    private static class LoadResult {

        public final Collection<DataPoint> points;
        public final Range                 range;
        public final ChartDataSource       source;

        public LoadResult(Collection<DataPoint> points, Range range, ChartDataSource source) {
            this.points = points;
            this.range = range;
            this.source = source;
        }
    }
}
