package tech.harmonysoft.android.leonardo.view.chart.legend;

import tech.harmonysoft.android.leonardo.model.ChartType;
import tech.harmonysoft.android.leonardo.model.DataPoint;
import tech.harmonysoft.android.leonardo.model.axis.AxisLabelTextStrategy;
import tech.harmonysoft.android.leonardo.model.axis.LabelType;
import tech.harmonysoft.android.leonardo.model.config.chart.ChartConfig;
import tech.harmonysoft.android.leonardo.model.data.ChartDataSource;
import tech.harmonysoft.android.leonardo.model.runtime.ChartModel;
import tech.harmonysoft.android.leonardo.model.text.TextWrapper;
import tech.harmonysoft.android.leonardo.util.LineUtil;
import tech.harmonysoft.android.leonardo.util.LongPropertyUtil;
import tech.harmonysoft.android.leonardo.view.chart.ChartDrawData;

import java.util.Map;
import java.util.NavigableSet;

public class LegendDrawUtil {

    private LegendDrawUtil() {
    }

    public static void fillValues(ChartModel model, Object dataAnchor, LegendDrawContext context) {
        if (!model.hasSelection()) {
            return;
        }
        for (ChartDataSource dataSource : model.getRegisteredDataSources()) {
            if (!model.isActive(dataSource)) {
                continue;
            }
            NavigableSet<DataPoint> points = model.getCurrentRangePoints(dataSource, dataAnchor);
            DataPoint previous = LongPropertyUtil.equalOrLower(model.getSelectedX(), points);
            if (previous == null) {
                continue;
            }
            if (previous.getX() == model.getSelectedX()) {
                context.dataSource2dataY.put(dataSource, previous.getY());
                continue;
            }

            DataPoint next = LongPropertyUtil.next(model.getSelectedX(), points);
            if (next == null) {
                continue;
            }

            float y = LineUtil.getY(previous.getX(), previous.getY(), next.getX(), next.getY(), model.getSelectedX());
            context.dataSource2dataY.put(dataSource, (long)y);
        }
    }

    public static void fillDimensionsData(LegendDrawContext context,
                                          ChartConfig config,
                                          ChartDrawData drawData,
                                          ChartModel model,
                                          ChartType chartType)
    {
        TextWrapper title = config.getXAxisConfig().getLabelTextStrategy().getLabel(model.getSelectedX(),
                                                                                    drawData.xAxis.axisStep,
                                                                                    LabelType.TITLE_SMALL);
        int maxWidth = drawData.getYLabelWidth(title) + drawData.getYLabelWidth(">");
        int height = 2 * context.verticalPadding
                        + Math.max(drawData.getYLabelHeight(title), drawData.getYLabelHeight(">"));

        AxisLabelTextStrategy textStrategy = config.getYAxisConfig().getLabelTextStrategy();
        for (Map.Entry<ChartDataSource, Long> entry : context.dataSource2dataY.entrySet()) {
            final long step;
            if (drawData.rightYDataSource == entry.getKey()) {
                step = drawData.rightYAxis.axisStep;
            } else {
                step = drawData.yAxis.axisStep;
            }
            int legendWidth = drawData.getYLabelWidth(entry.getKey().getLegend());
            if (chartType == ChartType.PERCENTAGE_STACKED) {
                legendWidth += drawData.getYLabelWidth("99% ");
            }
            TextWrapper valueLabel = textStrategy.getLabel(entry.getValue(), step, LabelType.NORMAL);
            int valueWidth = drawData.getYLabelWidth(valueLabel);
            maxWidth = Math.max(maxWidth, legendWidth + valueWidth);
            height += context.verticalPadding + Math.max(drawData.getYLabelHeight(entry.getKey().getLegend()),
                                                         drawData.getYLabelHeight(valueLabel));
        }

        if (chartType == ChartType.STACKED_BAR && context.dataSource2dataY.size() > 1) {
            height += context.verticalPadding + drawData.getYLabelHeight("All");

        }

        context.totalWidth = maxWidth + context.horizontalPadding * 5;
        context.totalHeight = height;
    }
}
