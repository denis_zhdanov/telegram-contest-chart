package tech.harmonysoft.android.leonardo.util;

public class DataPointAnchor implements WithComparableLongProperty {

    private long mValue;

    @Override
    public long getProperty() {
        return mValue;
    }

    public void setValue(long value) {
        mValue = value;
    }
}
