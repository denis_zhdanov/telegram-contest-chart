package tech.harmonysoft.android.leonardo.view.chart;

import android.graphics.Canvas;
import android.graphics.Paint;
import tech.harmonysoft.android.leonardo.model.ChartType;
import tech.harmonysoft.android.leonardo.model.Range;
import tech.harmonysoft.android.leonardo.model.config.chart.ChartConfig;
import tech.harmonysoft.android.leonardo.model.data.ChartDataSource;
import tech.harmonysoft.android.leonardo.model.runtime.ChartModel;
import tech.harmonysoft.android.leonardo.view.chart.legend.LegendDrawContext;

import javax.annotation.Nullable;
import java.util.List;
import java.util.NavigableMap;

public class StackedBarChartDrawer implements DataPointsDrawer {

    public static final StackedBarChartDrawer INSTANCE = new StackedBarChartDrawer();

    @Override
    public void draw(Canvas canvas,
                     ChartModel model,
                     ChartDrawData drawData,
                     ChartPalette palette,
                     ChartType chartType,
                     ChartConfig config,
                     Object dataAnchor)
    {
        Paint paint = palette.getPlotPaint();
        paint.setStyle(Paint.Style.FILL);

        List<ChartDataSource> ordered = drawData.getBarDataSources();

        NavigableMap<Long, BarSlice> slices = drawData.getSlices();
        Range xRange = drawData.xAxis.range;
        Long leftDataX = slices.floorKey(xRange.getStart());
        if (leftDataX == null) {
            leftDataX = slices.firstKey();
        }
        float minX = drawData.getChartLeft();
        while (leftDataX != null && leftDataX < xRange.getEnd()) {
            BarSlice slice = slices.get(leftDataX);
            if (slice == null) {
                leftDataX = slices.higherKey(leftDataX);
                continue;
            }
            float rightVisualX = drawData.dataXToVisualX(slice.getDataX() + slice.getXLength());
            if (rightVisualX < minX) {
                leftDataX = slices.higherKey(leftDataX);
                continue;
            }
            float leftVisualX = drawData.dataXToVisualX(slice.getDataX());
            if (leftVisualX < minX) {
                leftVisualX = minX;
            }
            float bottomVisualY = drawData.getChartBottom();
            for (ChartDataSource dataSource : ordered) {
                if (!model.isActive(dataSource)) {
                    continue;
                }

                Long dataY = slice.getDataY(dataSource);
                if (dataY == null) {
                    continue;
                }
                float height = drawData.getChartBottom() - drawData.dataYToVisualY(dataY);
                paint.setColor(dataSource.getPlotColor(config.isLight()));
                canvas.drawRect(leftVisualX, bottomVisualY - height, rightVisualX, bottomVisualY, paint);
                bottomVisualY -= height;
            }
            leftDataX = slices.higherKey(leftDataX);
        }
    }

    @SuppressWarnings("ConstantConditions")
    @Override
    public void initLegendLocation(LegendDrawContext context, ChartDrawData drawData, ChartModel model) {
        BarSlice selectedSlice = getSelectedSlice(drawData, model);
        if (selectedSlice == null) {
            return;
        }

        context.topOnChart = (drawData.getChartBottom() - context.totalHeight) / 2f;

        float sliceVisualXStart = drawData.dataXToVisualX(selectedSlice.getDataX());
        if (sliceVisualXStart >= context.totalWidth + context.horizontalPadding) {
            // We can locate the legend to the left of the selected slice
            context.leftOnChart = Math.max(0, sliceVisualXStart - context.totalWidth - context.horizontalPadding);
            return;
        }

        float sliceVisualXEnd = drawData.dataXToVisualX(selectedSlice.getDataX() + selectedSlice.getXLength());
        if (sliceVisualXEnd + context.horizontalPadding + context.totalWidth <= drawData.getChartRight()) {
            // We can locate the legend to the right of the selected slice
            context.leftOnChart = sliceVisualXEnd + context.horizontalPadding;
            return;
        }

        // The legend doesn't fit neither left nor right of the selection, let's draw it above
        int chartWidth = drawData.getChartRight() - drawData.getChartLeft();
        context.leftOnChart = drawData.getChartLeft() + (chartWidth - context.totalWidth) / 2f;
    }

    @Nullable
    private BarSlice getSelectedSlice(ChartDrawData drawData, ChartModel model) {
        if (!model.hasSelection()) {
            return null;
        }

        NavigableMap<Long, BarSlice> slices = drawData.getSlices();
        long selectionDataX = model.getSelectedX();
        Long sliceDataXStart = slices.floorKey(selectionDataX);
        if (sliceDataXStart == null) {
            return null;
        }

        return slices.get(sliceDataXStart);
    }

    @Override
    public void drawSelectionOverlay(ChartDrawData drawData,
                                     ChartModel model,
                                     Canvas canvas,
                                     ChartConfig config,
                                     ChartPalette palette)
    {
        BarSlice selectedSlice = getSelectedSlice(drawData, model);
        if (selectedSlice == null) {
            return;
        }

        Paint paint = palette.getBackgroundPaint();
        paint.setColor(config.getInactiveBarBackgroundColor());

        if (selectedSlice.getDataX() > drawData.xAxis.range.getEnd()
            || (selectedSlice.getDataX() + selectedSlice.getXLength() < drawData.xAxis.range.getStart()))
        {
            canvas.drawPaint(paint);
            return;
        }

        float sliceVisualXStart = drawData.dataXToVisualX(selectedSlice.getDataX());
        canvas.drawRect(drawData.getChartLeft(),
                        0,
                        sliceVisualXStart,
                        drawData.getChartBottom(),
                        paint);

        float sliceVisualXEnd = drawData.dataXToVisualX(selectedSlice.getDataX() + selectedSlice.getXLength());
        canvas.drawRect(sliceVisualXEnd,
                        0,
                        drawData.getChartRight(),
                        drawData.getChartBottom(),
                        paint);

        long dataHeight = 0L;
        for (Long dataY : selectedSlice.getDataYs().values()) {
            dataHeight += dataY;
        }
        canvas.drawRect(sliceVisualXStart, 0, sliceVisualXEnd, drawData.dataYToVisualY(dataHeight), paint);
    }
}
