package tech.harmonysoft.android.leonardo.util;

public class LineUtil {

    private LineUtil() {
    }

    public static float getX(float x1, float y1, float x2, float y2, float y) {
        float a = (y1 - y2) / (x1 - x2);
        float b = y1 - a * x1;
        return (y - b) / a;
    }

    public static float getY(float x1, float y1, float x2, float y2, float x) {
        float a = (y1 - y2) / (x1 - x2);
        float b = y1 - a * x1;
        return a * x + b;
    }
}
