package tech.harmonysoft.android.leonardo.model.config.axis.impl;

import tech.harmonysoft.android.leonardo.model.axis.AxisLabelTextStrategy;
import tech.harmonysoft.android.leonardo.model.axis.LabelType;
import tech.harmonysoft.android.leonardo.model.text.TextWrapper;

import javax.annotation.Nonnull;

/**
 * @author Denis Zhdanov
 * @since 12/3/19
 */
public class DefaultAxisLabelTextStrategy implements AxisLabelTextStrategy {

    public static final DefaultAxisLabelTextStrategy INSTANCE = new DefaultAxisLabelTextStrategy();

    private final TextWrapper mText = new TextWrapper();

    @Nonnull
    @Override
    public TextWrapper getLabel(long value, long step, LabelType type) {
        switch (type) {
            case MINIFIED:
                return getMinifiedLabel(value);
            default:
                return getLabel(value);
        }
    }

    @Nonnull
    private TextWrapper getLabel(long value) {
        mText.reset();
        int divisionsNumber = 0;
        long tmp = value;
        while (tmp >= 1000 && tmp % 1000 == 0) {
            tmp /= 1000;
            divisionsNumber++;
        }
        if (divisionsNumber == 0) {
            if (value > 1000 && value < 1000000 && value % 100 == 0) {
                mText.append(value / 1000);
                mText.append('.');
                mText.append((value / 100) % 10);
                mText.append('K');
                return mText;
            }
            long millions = value / 1000000;
            if (millions > 0) {
                mText.append(millions);
                mText.append(' ');
            }
            long thousands = (value % 1000000) / 1000;
            if (millions > 0) {
                addPadded(thousands);
            } else {
                mText.append(thousands);
            }
            mText.append(' ');
            addPadded(value % 1000);
            return mText;
        } else if (divisionsNumber == 1) {
            if (value > 1000000 && tmp % 100 == 0) {
                mText.append(tmp / 1000);
                mText.append('.');
                mText.append((tmp / 100) % 10);
                mText.append('M');
                return mText;
            }
            mText.append(tmp);
            mText.append("K");
            return mText;
        } else {
            mText.append(tmp);
            mText.append("M");
            return mText;
        }
    }

    @Nonnull
    private TextWrapper getMinifiedLabel(long value) {
        mText.reset();
        if (value < 1000) {
            mText.append(value);
            return mText;
        }

        if (value < 1000000) {
            if (value % 1000 != 0) {
                mText.append('~');
            }
            mText.append(Math.round(value / 1000d));
            mText.append('K');
            return mText;
        }

        if (value % 1000000 != 0) {
            mText.append('~');
        }
        mText.append(Math.round(value / 1000000d));
        mText.append('M');
        return mText;
    }

    private void addPadded(long value) {
        if (value < 10) {
            mText.append("00");
        } else if (value < 100) {
            mText.append('0');
        }
        mText.append(value);
    }
}
