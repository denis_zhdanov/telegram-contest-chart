package tech.harmonysoft.android.leonardo.model.config.data.impl;

import tech.harmonysoft.android.leonardo.model.DataPoint;
import tech.harmonysoft.android.leonardo.model.Range;
import tech.harmonysoft.android.leonardo.model.data.ChartDataLoader;
import tech.harmonysoft.android.leonardo.model.data.ChartDataSource;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Collection;

/**
 * @author Denis Zhdanov
 * @since 11/3/19
 */
public class ChartDataSourceImpl implements ChartDataSource {

    private final String          mLegend;
    private final Range           mDataRange;
    private final ChartDataLoader mLoader;
    private final int             mPlotLightColor;
    private final int             mLegendLightColor;
    private final int             mPlotDarkColor;
    private final int             mLegendDarkColor;
    private final int             mSelectorLightColor;
    private final int             mSelectorDarkColor;

    public ChartDataSourceImpl(String legend,
                               Range dataRange,
                               ChartDataLoader loader,
                               int plotLightColor,
                               int legendLightColor,
                               int plotDarkColor,
                               int legendDarkColor,
                               int selectorLightColor,
                               int selectorDarkColor)
    {
        mLegend = legend;
        mDataRange = dataRange;
        mLoader = loader;
        mPlotLightColor = plotLightColor;
        mLegendLightColor = legendLightColor;
        mPlotDarkColor = plotDarkColor;
        mLegendDarkColor = legendDarkColor;
        mSelectorLightColor = selectorLightColor;
        mSelectorDarkColor = selectorDarkColor;
    }

    @Nonnull
    @Override
    public String getLegend() {
        return mLegend;
    }

    @Override
    public int getPlotColor(boolean light) {
        return light ? mPlotLightColor : mPlotDarkColor;
    }

    @Override
    public int getLegendColor(boolean light) {
        return light ? mLegendLightColor : mLegendDarkColor;
    }

    @Override
    public int getSelectorColor(boolean light) {
        return light ? mSelectorLightColor : mSelectorDarkColor;
    }

    @Nonnull
    @Override
    public Range getDataRange() {
        return mDataRange;
    }

    @Nullable
    @Override
    public Collection<DataPoint> load(Range range) {
        return mLoader.load(range);
    }

    @Nonnull
    @Override
    public String toString() {
        return mLegend;
    }
}
