package tech.harmonysoft.android.leonardo.model.config.navigator;

import javax.annotation.Nonnull;

public interface NavigatorConfigProvider {

    @Nonnull
    NavigatorConfig buildConfig();
}
