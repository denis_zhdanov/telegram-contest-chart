package tech.harmonysoft.android.leonardo.view.util;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;

/**
 * @author Denis Zhdanov
 * @since 16/3/19
 */
public class RoundedRectangleDrawer {

    public static final RoundedRectangleDrawer INSTANCE = new RoundedRectangleDrawer();

    private final RectF mRect = new RectF();

    public void draw(RectF rect, Paint borderPaint, Paint fillPaint, float radius, Canvas canvas) {
        drawBackground(rect, fillPaint, radius, borderPaint.getStrokeWidth(), canvas);
        drawEdges(rect, borderPaint, radius, borderPaint.getStrokeWidth(), canvas);
        drawRoundedEdges(rect, borderPaint, radius, borderPaint.getStrokeWidth(), canvas);
    }

    private void drawBackground(RectF rect, Paint paint, float radius, float strokeWidth, Canvas canvas) {
        // Top-left corner
        canvas.drawCircle(rect.left + radius, rect.top + radius, radius - strokeWidth, paint);

        // Top-right corner
        canvas.drawCircle(rect.right - radius, rect.top + radius, radius - strokeWidth, paint);

        // Bottom-right corner
        canvas.drawCircle(rect.right - radius, rect.bottom - radius, radius - strokeWidth, paint);

        // Bottom-left corner
        canvas.drawCircle(rect.left + radius, rect.bottom - radius, radius - strokeWidth, paint);

        // Center area
        canvas.drawRect(rect.left + radius + strokeWidth,
                        rect.top + strokeWidth,
                        rect.right - radius - strokeWidth,
                        rect.bottom - strokeWidth,
                        paint);

        // Left area
        canvas.drawRect(rect.left + strokeWidth,
                        rect.top + radius + strokeWidth,
                        rect.left + radius + strokeWidth,
                        rect.bottom - radius - strokeWidth,
                        paint);

        // Right area
        canvas.drawRect(rect.right - radius - strokeWidth,
                        rect.top + radius + strokeWidth,
                        rect.right - strokeWidth,
                        rect.bottom - radius - strokeWidth,
                        paint);
    }

    private void drawEdges(RectF rect, Paint paint, float radius, float strokeWidth, Canvas canvas) {
        // Left edge
        canvas.drawLine(rect.left + strokeWidth,
                        rect.top + radius + strokeWidth,
                        rect.left + strokeWidth,
                        rect.bottom - radius - strokeWidth,
                        paint);

        // Top edge
        canvas.drawLine(rect.left + radius + strokeWidth,
                        rect.top + strokeWidth,
                        rect.right - radius - strokeWidth,
                        rect.top + strokeWidth,
                        paint);

        // Right edge
        canvas.drawLine(rect.right - strokeWidth,
                        rect.top + radius + strokeWidth,
                        rect.right - strokeWidth,
                        rect.bottom - radius - strokeWidth,
                        paint);

        // Bottom edge
        canvas.drawLine(rect.left + radius + strokeWidth,
                        rect.bottom - strokeWidth,
                        rect.right - radius - strokeWidth,
                        rect.bottom - strokeWidth,
                        paint);
    }

    private void drawRoundedEdges(RectF rect, Paint paint, float radius, float strokeWidth, Canvas canvas) {
        // Top-left corner
        mRect.set(rect.left + strokeWidth,
                  rect.top + strokeWidth,
                  rect.left + 2 * radius + strokeWidth,
                  rect.top + 2 * radius + strokeWidth);
        canvas.drawArc(mRect, 180, 90, false, paint);

        // Top-right corner
        mRect.set(rect.right - 2 * radius - strokeWidth,
                  rect.top + strokeWidth,
                  rect.right - strokeWidth,
                  rect.top + 2 * radius + strokeWidth);
        canvas.drawArc(mRect, 270, 90, false, paint);

        // Bottom-right corner
        mRect.set(rect.right - 2 * radius - strokeWidth,
                  rect.bottom - 2 * radius - strokeWidth,
                  rect.right - strokeWidth,
                  rect.bottom - strokeWidth);
        canvas.drawArc(mRect, 0, 90, false, paint);

        // Bottom-left corner
        mRect.set(rect.left + strokeWidth,
                  rect.bottom - 2 * radius - strokeWidth,
                  rect.left + 2 * radius + strokeWidth,
                  rect.bottom - strokeWidth);
        canvas.drawArc(mRect, 90, 90, false, paint);
    }
}
