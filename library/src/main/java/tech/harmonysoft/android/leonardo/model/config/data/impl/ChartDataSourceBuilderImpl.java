package tech.harmonysoft.android.leonardo.model.config.data.impl;

import tech.harmonysoft.android.leonardo.model.Range;
import tech.harmonysoft.android.leonardo.model.config.data.ChartDataSourceBuilder;
import tech.harmonysoft.android.leonardo.model.data.ChartDataLoader;
import tech.harmonysoft.android.leonardo.model.data.ChartDataSource;

import javax.annotation.Nonnull;

/**
 * @author Denis Zhdanov
 * @since 11/3/19
 */
public class ChartDataSourceBuilderImpl implements ChartDataSourceBuilder {

    private long mMinX = Long.MIN_VALUE;
    private long mMaxX = Long.MAX_VALUE;

    private ChartDataLoader mLoader;
    private String          mLegend;
    private int             mPlotLightColor;
    private int             mLegendLightColor;
    private int             mSelectorLightColor;
    private int             mPlotDarkColor;
    private int             mLegendDarkColor;
    private int             mSelectorDarkColor;

    @Nonnull
    @Override
    public ChartDataSourceBuilder withMinX(long minX) {
        mMinX = minX;
        return this;
    }

    @Nonnull
    @Override
    public ChartDataSourceBuilder withMaxX(long maxX) {
        mMaxX = maxX;
        return this;
    }

    @Nonnull
    @Override
    public ChartDataSourceBuilder withLegend(String legend) {
        mLegend = legend;
        return this;
    }

    @Nonnull
    @Override
    public ChartDataSourceBuilder withLoader(ChartDataLoader loader) {
        mLoader = loader;
        return this;
    }

    @Nonnull
    @Override
    public ChartDataSourceBuilder withPlotColor(int color, boolean lightTheme) {
        if (lightTheme) {
            mPlotLightColor = color;
        } else {
            mPlotDarkColor = color;
        }
        return this;
    }

    @Nonnull
    @Override
    public ChartDataSourceBuilder withLegendColor(int color, boolean lightTheme) {
        if (lightTheme) {
            mLegendLightColor = color;
        } else {
            mLegendDarkColor = color;
        }
        return this;
    }

    @Nonnull
    @Override
    public ChartDataSourceBuilder withSelectorColor(int color, boolean lightTheme) {
        if (lightTheme) {
            mSelectorLightColor = color;
        } else {
            mSelectorDarkColor = color;
        }
        return this;
    }

    @Nonnull
    @Override
    public ChartDataSource build() {
        // Fixate values
        long minX = mMinX;
        long maxX = mMaxX;
        if (minX > maxX) {
            throw new IllegalStateException(String.format("Min X (%d) is greater than max X (%d)", minX, maxX));
        }

        String legend = mLegend;
        if (legend == null) {
            throw new IllegalStateException("Legend is undefined");
        }

        ChartDataLoader loader = mLoader;
        if (loader == null) {
            throw new IllegalStateException("Data loader is undefined");
        }

        return new ChartDataSourceImpl(legend,
                                       new Range(minX, maxX),
                                       loader,
                                       mPlotLightColor,
                                       mLegendLightColor,
                                       mPlotDarkColor,
                                       mLegendDarkColor,
                                       mSelectorLightColor,
                                       mSelectorDarkColor);
    }
}
