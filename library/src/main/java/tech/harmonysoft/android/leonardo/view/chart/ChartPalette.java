package tech.harmonysoft.android.leonardo.view.chart;

import android.graphics.Paint;
import android.graphics.Typeface;
import tech.harmonysoft.android.leonardo.model.config.axis.AxisConfig;
import tech.harmonysoft.android.leonardo.model.config.chart.ChartConfig;

import javax.annotation.Nonnull;

class ChartPalette {

    private final ChartConfig mConfig;
    private final Paint       mBackgroundPaint;
    private final Paint       mGridPaint;
    private final Paint       mPlotPaint;
    private final Paint       mLegendValuePaint;
    private final Paint       mXLabelPaint;
    private final Paint       mYLabelPaint;

    public ChartPalette(ChartConfig config) {
        mConfig = config;
        mBackgroundPaint = buildBackgroundPaint(config);
        mGridPaint = buildGridPaint(config);
        mPlotPaint = buildPlotPaint(config);
        mLegendValuePaint = buildLegendValuePaint(config);
        mXLabelPaint = buildXLabelPaint(config);
        mYLabelPaint = buildYLabelPaint(config);
    }

    @Nonnull
    private Paint buildBackgroundPaint(ChartConfig config) {
        Paint paint = new Paint();
        paint.setColor(config.getBackgroundColor());
        paint.setStyle(Paint.Style.FILL);
        return paint;
    }

    @Nonnull
    private Paint buildGridPaint(ChartConfig config) {
        Paint paint = new Paint();
        paint.setColor(config.getGridColor());
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(config.getGridLineWidthInPixels());
        return paint;
    }

    @Nonnull
    private Paint buildPlotPaint(ChartConfig config) {
        Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        paint.setStrokeWidth(config.getPlotLineWidthInPixels());
        return paint;
    }

    @Nonnull
    private Paint buildLegendValuePaint(ChartConfig config) {
        Paint paint = new Paint();
        paint.setTypeface(Typeface.DEFAULT);
        paint.setTextSize(config.getYAxisConfig().getFontSizeInPixels() * 3 / 2);
        return paint;
    }

    @Nonnull
    private Paint buildXLabelPaint(ChartConfig config) {
        return buildAxisLabelPaint(config.getXAxisConfig());
    }

    @Nonnull
    private Paint buildYLabelPaint(ChartConfig config) {
        return buildAxisLabelPaint(config.getYAxisConfig());
    }

    @Nonnull
    private Paint buildAxisLabelPaint(AxisConfig config) {
        Paint paint = new Paint();
        paint.setColor(config.getLabelColor());
        paint.setTypeface(Typeface.DEFAULT);
        paint.setTextSize(config.getFontSizeInPixels());
        return paint;
    }

    @Nonnull
    public Paint getBackgroundPaint() {
        mBackgroundPaint.setColor(mConfig.getBackgroundColor());
        mBackgroundPaint.setStyle(Paint.Style.FILL);
        return mBackgroundPaint;
    }

    @Nonnull
    public Paint getGridPaint() {
        mGridPaint.setColor(mConfig.getGridColor());
        mGridPaint.setStyle(Paint.Style.STROKE);
        mGridPaint.setStrokeWidth(mConfig.getGridLineWidthInPixels());
        return mGridPaint;
    }

    @Nonnull
    public Paint getPlotPaint() {
        mPlotPaint.setStyle(Paint.Style.STROKE);
        mPlotPaint.setStrokeWidth(mConfig.getPlotLineWidthInPixels());
        mPlotPaint.setAlpha(255);
        return mPlotPaint;
    }

    @Nonnull
    public Paint getLegendValuePaint() {
        mLegendValuePaint.setTypeface(Typeface.DEFAULT);
        mLegendValuePaint.setTextSize(mConfig.getYAxisConfig().getFontSizeInPixels() * 3f / 2f);
        return mLegendValuePaint;
    }

    @Nonnull
    public Paint getXLabelPaint() {
        prepareAxisPaint(mXLabelPaint, mConfig.getXAxisConfig());
        return mXLabelPaint;
    }

    @Nonnull
    public Paint getYLabelPaint() {
        prepareAxisPaint(mYLabelPaint, mConfig.getYAxisConfig());
        return mYLabelPaint;
    }

    private void prepareAxisPaint(Paint paint, AxisConfig config) {
        paint.setColor(config.getLabelColor());
        paint.setStyle(Paint.Style.FILL);
        paint.setTextSize(config.getFontSizeInPixels());
        paint.setTypeface(Typeface.DEFAULT);
        paint.setAlpha(255);
    }
}
