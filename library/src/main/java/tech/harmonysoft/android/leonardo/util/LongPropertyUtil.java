package tech.harmonysoft.android.leonardo.util;

import javax.annotation.Nullable;
import java.util.NavigableSet;

@SuppressWarnings("unchecked")
public class LongPropertyUtil {

    private static DataPointAnchor ANCHOR = new DataPointAnchor();

    private LongPropertyUtil() {
    }

    @Nullable
    public static <T extends WithComparableLongProperty> T previous(long bound, NavigableSet<T> points) {
        ANCHOR.setValue(bound);
        WithComparableLongProperty floor = ((NavigableSet<WithComparableLongProperty>) points).floor(ANCHOR);
        if (floor == null) {
            return null;
        } else if (floor.getProperty() < bound) {
            return (T) floor;
        } else {
            return points.lower((T) floor);
        }
    }

    @Nullable
    public static <T extends WithComparableLongProperty> T equalOrLower(long bound, NavigableSet<T> points) {
        ANCHOR.setValue(bound);
        return (T) ((NavigableSet<WithComparableLongProperty>) points).floor(ANCHOR);
    }

    @Nullable
    public static <T extends WithComparableLongProperty> T next(long bound, NavigableSet<T> points) {
        ANCHOR.setValue(bound);
        WithComparableLongProperty ceiling = ((NavigableSet<WithComparableLongProperty>) points).ceiling(ANCHOR);
        if (ceiling == null) {
            return null;
        } else if (ceiling.getProperty() > bound) {
            return (T) ceiling;
        } else {
            return points.higher((T) ceiling);
        }
    }

    public static <T extends WithComparableLongProperty> T equalOrHigher(long bound, NavigableSet<T> points) {
        ANCHOR.setValue(bound);
        return (T) ((NavigableSet<WithComparableLongProperty>) points).ceiling(ANCHOR);
    }
}
