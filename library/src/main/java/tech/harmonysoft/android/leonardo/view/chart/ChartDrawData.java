package tech.harmonysoft.android.leonardo.view.chart;

import android.util.LongSparseArray;
import tech.harmonysoft.android.leonardo.model.ChartType;
import tech.harmonysoft.android.leonardo.model.DataPoint;
import tech.harmonysoft.android.leonardo.model.Range;
import tech.harmonysoft.android.leonardo.model.VisualPoint;
import tech.harmonysoft.android.leonardo.model.config.chart.ChartConfig;
import tech.harmonysoft.android.leonardo.model.data.ChartDataSource;
import tech.harmonysoft.android.leonardo.model.runtime.ChartModel;
import tech.harmonysoft.android.leonardo.model.runtime.ChartModelListener;
import tech.harmonysoft.android.leonardo.model.runtime.DataMapper;
import tech.harmonysoft.android.leonardo.model.text.TextWrapper;
import tech.harmonysoft.android.leonardo.util.LeonardoUtil;
import tech.harmonysoft.android.leonardo.view.util.*;

import javax.annotation.Nonnull;
import java.util.*;

public class ChartDrawData implements DataMapper {

    public final AxisStepChooser axisStepChooser = AxisStepChooser.INSTANCE;

    private static final int BARS_ON_SCREEN_NUMBER = 28;
    private static final int PERCENTAGE_BARS_ON_SCREEN_NUMBER = 200;

    private static final Range PERCENTAGE_Y_RANGE = new Range(0, 100);

    public final ChartAxisData   xAxis;
    public final ChartAxisData   yAxis;
    public final ChartAxisData   rightYAxis;
    public final ChartDataSource leftYDataSource;
    public final ChartDataSource rightYDataSource;
    public final int             legendLabelHeight;

    public int maxYLabelWidth;
    public int maxRightYLabelWidth;

    private final NavigableMap<Long, BarSlice> mSlices         = new TreeMap<>();
    private final List<ChartDataSource>        mBarDataSources = new ArrayList<>();

    private final TextSpaceMeasurer mYLabelHeightMeasurer = new TextSpaceMeasurer() {
        @Override
        public int measureVisualSpace(String text) {
            return yAxis.labelHeight;
        }

        @Override
        public int measureVisualSpace(TextWrapper text) {
            return yAxis.labelHeight;
        }
    };

    private final ChartView          mView;
    private final ChartType          mChartType;
    private final ChartModel         mModel;
    private final TextWidthMeasurer  mXWidthMeasurer;
    private final TextWidthMeasurer  mYWidthMeasurer;
    private final TextHeightMeasurer mYHeightMeasurer;
    private final TextWidthMeasurer  mLegendWidthMeasurer;
    private final PlotAnimator       mPlotAnimator;
    private final boolean            mDrawXLabels;
    private final boolean            mAnimationEnabled;

    private boolean mForceRefresh;
    private int     mLastWidth;
    private int     mLastHeight;

    public ChartDrawData(ChartView view, ChartModel model, ChartType chartType, ChartConfig config) {
        mView = view;
        mModel = model;
        mChartType = chartType;

        ChartPalette palette = new ChartPalette(config);
        xAxis = new ChartAxisData(palette.getXLabelPaint(),
                                  config.getXAxisConfig(),
                                  new AxisAnimator(view, LeonardoUtil.ANIMATION_DURATION_MILLIS));
        yAxis = new ChartAxisData(palette.getYLabelPaint(),
                                  config.getYAxisConfig(),
                                  new AxisAnimator(view, LeonardoUtil.ANIMATION_DURATION_MILLIS));
        rightYAxis = new ChartAxisData(palette.getYLabelPaint(),
                                       config.getYAxisConfig(),
                                       new AxisAnimator(view, LeonardoUtil.ANIMATION_DURATION_MILLIS));
        legendLabelHeight = new TextHeightMeasurer(palette.getLegendValuePaint()).measureVisualSpace("W");
        mXWidthMeasurer = new TextWidthMeasurer(palette.getXLabelPaint());
        mYWidthMeasurer = new TextWidthMeasurer(palette.getYLabelPaint());
        mYHeightMeasurer = new TextHeightMeasurer(palette.getYLabelPaint());
        mLegendWidthMeasurer = new TextWidthMeasurer(palette.getLegendValuePaint());
        mPlotAnimator = new PlotAnimator(view, LeonardoUtil.ANIMATION_DURATION_MILLIS);
        if (chartType == ChartType.LINE_2_Y) {
            Iterator<ChartDataSource> iterator = model.getRegisteredDataSources().iterator();
            leftYDataSource = iterator.next();
            rightYDataSource = iterator.next();
        } else {
            leftYDataSource = null;
            rightYDataSource = null;
        }

        mDrawXLabels = config.getXAxisConfig().shouldDrawAxis() && config.getXAxisConfig().shouldDrawLabels();
        mAnimationEnabled = config.isAnimationEnabled();

        model.addListener(new ChartModelListener() {
            @Override
            public void onRangeChanged(Object anchor) {

            }

            @Override
            public void onDataSourceEnabled(ChartDataSource dataSource) {

            }

            @Override
            public void onDataSourceDisabled(ChartDataSource dataSource) {

            }

            @Override
            public void onDataSourceAdded(ChartDataSource dataSource) {

            }

            @Override
            public void onDataSourceRemoved(ChartDataSource dataSource) {

            }

            @Override
            public void onActiveDataPointsLoaded(Object anchor) {
                if (anchor == view.getDataAnchor()) {
                    mForceRefresh = true;
                }
            }

            @Override
            public void onSelectionChange() {

            }
        });
    }

    public int getChartBottom() {
        if (mDrawXLabels) {
            return mView.getHeight() - xAxis.labelHeight - xAxis.labelPadding;
        } else {
            return mView.getHeight();
        }
    }

    public int getChartLeft() {
        return maxYLabelWidth <= 0 ? 0 : maxYLabelWidth + yAxis.labelPadding;
    }

    public int getChartRight() {
        int result = mView.getWidth();
        if (maxRightYLabelWidth > 0) {
            result -= maxRightYLabelWidth + rightYAxis.labelPadding;
        }
        return result;
    }

    @Nonnull
    public NavigableMap<Long, BarSlice> getSlices() {
        return mSlices;
    }

    @Nonnull
    public List<ChartDataSource> getBarDataSources() {
        return mBarDataSources;
    }

    public void refresh() {
        int currentWidth = mView.getWidth();
        int currentHeight = mView.getHeight();
        Range activeXDataRange = mModel.getActiveRange(mView.getDataAnchor());
        if (!mForceRefresh
            && mLastWidth == currentWidth
            && mLastHeight == currentHeight
            && activeXDataRange == xAxis.range)
        {
            return;
        }

        mLastWidth = currentWidth;
        mLastHeight = currentHeight;

        Range activeYDataRange = mayBeExpandYRange(getYDataRange(true));
        refreshAxis(activeYDataRange, getChartBottom(), yAxis, true);

        refreshAxis(activeXDataRange, currentWidth - maxYLabelWidth - yAxis.labelPadding, xAxis, false);

        mForceRefresh = false;
        mayBeInitData();
    }

    private void mayBeInitData() {
        if ((mChartType != ChartType.STACKED_BAR && mChartType != ChartType.PERCENTAGE_STACKED)
            || !mBarDataSources.isEmpty())
        {
            return;
        }

        initDataSources();
        initSlices(mChartType == ChartType.STACKED_BAR ? BARS_ON_SCREEN_NUMBER : PERCENTAGE_BARS_ON_SCREEN_NUMBER);
    }

    private void initDataSources() {
        NavigableMap<Float, ChartDataSource> avgY2DataSource = new TreeMap<>();
        for (ChartDataSource dataSource : mModel.getRegisteredDataSources()) {
            float sum = 0f;
            int count = 0;
            mModel.setActiveRange(dataSource.getDataRange(), this);
            for (DataPoint point : mModel.getCurrentRangePoints(dataSource, this)) {
                sum += point.getY();
                count++;
            }
            avgY2DataSource.put(sum / count, dataSource);
        }
        for (Map.Entry<Float, ChartDataSource> entry : avgY2DataSource.entrySet()) {
            mBarDataSources.add(entry.getValue());
        }
        Collections.reverse(mBarDataSources);
    }

    private void initSlices(int barsOnScreen) {
        long startDataX = Long.MAX_VALUE;
        long endDataX = Long.MIN_VALUE;
        for (ChartDataSource dataSource : mModel.getRegisteredDataSources()) {
            mModel.setActiveRange(dataSource.getDataRange(), this);
            NavigableSet<DataPoint> points = mModel.getCurrentRangePoints(dataSource, this);

            DataPoint first = points.first();
            if (first.getX() < startDataX) {
                startDataX = first.getX();
            }

            DataPoint last = points.last();
            if (last.getX() > endDataX) {
                endDataX = last.getX();
            }
        }

        long step = xAxis.range.getSize() / barsOnScreen;

        for (; startDataX < endDataX; startDataX += step) {
            long sliceEnd = startDataX + step;
            Map<ChartDataSource, Long> dataYs = new HashMap<>();
            mModel.setActiveRange(new Range(startDataX, sliceEnd), this);
            for (ChartDataSource dataSource : mModel.getRegisteredDataSources()) {
                NavigableSet<DataPoint> points = mModel.getCurrentRangePoints(dataSource, this);
                long ySum = 0L;
                long count = 0;
                for (DataPoint point : points) {
                    ySum += point.getY();
                    count++;
                }
                if (count > 0) {
                    dataYs.put(dataSource, ySum / count);
                }
            }
            if (!dataYs.isEmpty()) {
                mSlices.put(startDataX, new BarSlice(dataYs, startDataX, sliceEnd - startDataX));
            }
        }
    }

    @Nonnull
    private Range getYDataRange(boolean left) {
        switch (mChartType) {
            case LINE:
            case LINE_2_Y:
                return getLineYDataRange(left);
            case STACKED_BAR:
                return getStackedBarYDataRange();
            case PERCENTAGE_STACKED:
                return PERCENTAGE_Y_RANGE;
            default:
                return getLineYDataRange(left);
        }
    }

    @Nonnull
    private Range getLineYDataRange(boolean left) {
        long minY = Long.MAX_VALUE;
        long maxY = Long.MIN_VALUE;
        for (ChartDataSource dataSource : mModel.getRegisteredDataSources()) {
            if (mChartType == ChartType.LINE_2_Y) {
                if ((left && dataSource == rightYDataSource) || (!left && dataSource == leftYDataSource)) {
                    continue;
                }
            } else if (!mModel.isActive(dataSource)) {
                continue;
            }
            for (DataPoint point : mModel.getCurrentRangePoints(dataSource, mView.getDataAnchor())) {
                long y = point.getY();
                if (y < minY) {
                    minY = y;
                }
                if (y > maxY) {
                    maxY = y;
                }
            }
        }

        return minY <= maxY ? new Range(minY, maxY) : Range.EMPTY_RANGE;
    }

    @Nonnull
    private Range getStackedBarYDataRange() {
        LongSparseArray<Long> values = new LongSparseArray<>();
        for (ChartDataSource dataSource : mModel.getRegisteredDataSources()) {
            if (!mModel.isActive(dataSource)) {
                continue;
            }
            for (DataPoint point : mModel.getCurrentRangePoints(dataSource, mView.getDataAnchor())) {
                Long y = values.get(point.getX());
                if (y == null) {
                    values.put(point.getX(), point.getY());
                } else {
                    values.put(point.getX(), y + point.getY());
                }
            }
        }
        if (values.size() <= 0) {
            return Range.EMPTY_RANGE;
        }
        long maxY = Long.MIN_VALUE;
        for (int i = 0, max = values.size(); i < max; i++) {
            long x = values.keyAt(i);
            long y = values.get(x);
            if (y > maxY) {
                maxY = y;
            }
        }
        return new Range(0L, maxY);
    }

    @Nonnull
    private Range mayBeExpandYRange(Range range) {
        if (range == PERCENTAGE_Y_RANGE || yAxis.axisStep <= 0 || !yAxis.drawAxis) {
            return range;
        } else {
            return range.padBy(yAxis.axisStep);
        }
    }

    private void refreshAxis(Range currentDataRange, int availableSize, ChartAxisData data, boolean yAxis) {
        boolean rescale = (currentDataRange.getSize() != data.range.getSize()) || (availableSize != data.availableSize);
        Range initialRange = data.range;
        data.range = currentDataRange;

        if (!rescale) {
            if (yAxis && mChartType == ChartType.LINE_2_Y) {
                Range rightYDataRange = getYDataRange(false);
                if (!rightYDataRange.equals(rightYAxis.range)) {
                    rightYAxis.availableSize = availableSize;
                    rightYAxis.animator.rangeFrom = rightYAxis.range;
                    rightYAxis.range = rightYDataRange;
                    rightYAxis.animator.rangeTo = rightYAxis.range;
                    rightYAxis.animator.visualSize = availableSize;
                    rightYAxis.unitSize = ((float) availableSize) / rightYAxis.range.getSize();
                }
            }
            return;
        }

        long initialStep = data.axisStep;
        long initialSize = data.availableSize;

        if (yAxis) {
            maxYLabelWidth = 0;
        }

        data.availableSize = availableSize;
        data.visualShift = 0f;

        Range rangeForAxisStepCalculation = yAxis ? getYDataRange(true) : currentDataRange;

        AxisStepChooser.MinGapStrategy gapStrategy = yAxis ? YAxisLabelGapStrategy.INSTANCE
                                                           : XAxisLabelGapStrategy.INSTANCE;

        TextSpaceMeasurer measurer = yAxis ? mYLabelHeightMeasurer : mXWidthMeasurer;

        if (yAxis && mChartType == ChartType.PERCENTAGE_STACKED) {
            data.axisStep = 25;
        } else {
            data.axisStep = axisStepChooser.choose(data.labelTextStrategy,
                                                   gapStrategy,
                                                   rangeForAxisStepCalculation,
                                                   availableSize,
                                                   measurer);
        }

        if (yAxis) {
            data.range = mayBeExpandYRange(rangeForAxisStepCalculation);
            if (mChartType == ChartType.LINE_2_Y) {
                rightYAxis.availableSize = availableSize;
                rightYAxis.animator.rangeFrom = rightYAxis.range;
                rightYAxis.range = getYDataRange(false);
                rightYAxis.animator.rangeTo = rightYAxis.range;
                rightYAxis.animator.visualSize = availableSize;
                rightYAxis.unitSize = ((float) availableSize) / rightYAxis.range.getSize();
            }
        }

        data.unitSize = ((float) availableSize) / data.range.getSize();

        if (yAxis && mAnimationEnabled && initialSize > 0) {
            data.animator.animate(initialRange, data.range, initialStep, availableSize);
        }
    }

    public void onYLabel(TextWrapper label) {
        int width = mYWidthMeasurer.measureVisualSpace(label);
        if (width > maxYLabelWidth) {
            maxYLabelWidth = width;
        }
    }

    public int getYLabelWidth(String label) {
        return mYWidthMeasurer.measureVisualSpace(label);
    }

    public int getYLabelWidth(TextWrapper label) {
        return mYWidthMeasurer.measureVisualSpace(label);
    }

    public int getYLabelHeight(TextWrapper label) {
        return mYHeightMeasurer.measureVisualSpace(label);
    }

    public int getYLabelHeight(String label) {
        return mYHeightMeasurer.measureVisualSpace(label);
    }

    public int getLegendValueWidth(TextWrapper value) {
        return mLegendWidthMeasurer.measureVisualSpace(value);
    }

    public void fadeIn(ChartDataSource dataSource) {
        mPlotAnimator.fadeIn(dataSource);
        mForceRefresh = true;
    }

    public void fadeOut(ChartDataSource dataSource) {
        mPlotAnimator.fadeOut(dataSource);
        mForceRefresh = true;
    }

    public boolean isAnimationInProgress(ChartDataSource dataSource) {
        return mPlotAnimator.isAnimationInProgress(dataSource);
    }

    public int getCurrentAlpha(ChartDataSource dataSource) {
        return mPlotAnimator.getAlpha(dataSource);
    }

    @Override
    public float dataXToVisualX(long dataX) {
        refresh();
        if (xAxis.animator.isInProgress()) {
            return xAxis.visualShift + xAxis.animator.getVisualValue(dataX) + getChartLeft();
        } else {
            return xAxis.dataValueToVisualValue(dataX) + getChartLeft();
        }
    }

    @Override
    public long visualXToDataX(float visualX) {
        refresh();
        if (xAxis.animator.isInProgress()) {
            return xAxis.animator.getDataValue(visualX);
        } else {
            return xAxis.visualValueToDataValue(visualX - getChartLeft());
        }
    }

    public float dataYToVisualY(long dataY, ChartDataSource dataSource) {
        if (mChartType == ChartType.LINE_2_Y && dataSource == rightYDataSource) {
            final float diff;
            if (yAxis.animator.isInProgress()) {
                rightYAxis.animator.elapsedTimeRatio = yAxis.animator.elapsedTimeRatio;
                diff = rightYAxis.animator.getVisualValue(dataY);
            } else {
                diff = rightYAxis.dataValueToVisualValue(dataY);
            }
            return rightYAxis.availableSize - diff;
        } else {
            return dataYToVisualY(dataY);
        }
    }

    public float dataYToVisualY(long dataY) {
        refresh();
        final float diff;
        if (yAxis.animator.isInProgress()) {
            diff = yAxis.animator.getVisualValue(dataY);
        } else {
            diff = yAxis.dataValueToVisualValue(dataY);
        }
        return yAxis.availableSize - diff;
    }

    public long visualYToDataY(float visualY, ChartDataSource dataSource) {
        if (mChartType == ChartType.LINE_2_Y && dataSource == rightYDataSource) {
            if (yAxis.animator.isInProgress()) {
                rightYAxis.animator.elapsedTimeRatio = yAxis.animator.elapsedTimeRatio;
                return rightYAxis.animator.getDataValue(rightYAxis.availableSize - visualY);
            } else {
                return rightYAxis.visualValueToDataValue(rightYAxis.availableSize - visualY);
            }
        } else {
            return yAxis.visualValueToDataValue(yAxis.availableSize - visualY);
        }
    }

    @Nonnull
    @Override
    public VisualPoint dataPointToVisualPoint(DataPoint dataPoint) {
        return new VisualPoint(dataXToVisualX(dataPoint.getX()), dataYToVisualY(dataPoint.getY()));
    }
}
