package tech.harmonysoft.android.leonardo.view.chart;

import android.animation.ValueAnimator;
import android.view.View;
import android.view.animation.LinearInterpolator;
import tech.harmonysoft.android.leonardo.model.Range;

class AxisAnimator {

    private final ValueAnimator mAnimator = new ValueAnimator();

    private final View mView;
    private final long mAnimationDurationMillis;
    private final long mFadeDurationMs;
    private final long mFadeDelay;

    public int initialStepAlpha = 255;
    public int finalStepAlpha   = 0;

    public Range rangeFrom = Range.EMPTY_RANGE;
    public Range rangeTo   = Range.EMPTY_RANGE;

    public long  initialStep;
    public float visualSize;
    public float elapsedTimeRatio;

    public AxisAnimator(View view, long animationDurationMillis) {
        mView = view;
        mAnimationDurationMillis = animationDurationMillis;
        mFadeDurationMs = animationDurationMillis * 2 / 3;
        mFadeDelay = animationDurationMillis - mFadeDurationMs;

        setUpAnimator();
    }

    private void setUpAnimator() {
        mAnimator.setInterpolator(new LinearInterpolator());
        mAnimator.setDuration(mAnimationDurationMillis);
        mAnimator.addUpdateListener(animation -> tickAnimation((Float) animation.getAnimatedValue()));
    }

    public boolean isInProgress() {
        return mAnimator.isRunning();
    }

    public void animate(Range from, Range to, long initialStep, int visualSize) {
        if (isInProgress()) {
            rangeFrom = new Range(
                    (long) (rangeFrom.getStart() + ((rangeTo.getStart() - rangeFrom.getStart()) * elapsedTimeRatio)),
                    (long) (rangeFrom.getEnd() + ((rangeTo.getEnd() - rangeFrom.getEnd()) * elapsedTimeRatio))
            );
        } else {
            rangeFrom = from;
        }
        this.initialStep = initialStep;
        this.visualSize = visualSize;
        rangeTo = to;

        mAnimator.cancel();
        mAnimator.setFloatValues(0f, mAnimationDurationMillis);
        initialStepAlpha = 255;
        finalStepAlpha = 0;
        mAnimator.start();
    }

    private void tickAnimation(float elapsedTimeMs) {
        elapsedTimeRatio = elapsedTimeMs / mAnimationDurationMillis;

        if (elapsedTimeMs <= mFadeDelay) {
            return;
        }

        finalStepAlpha = (int) ((elapsedTimeMs - mFadeDelay) * 255 / mFadeDurationMs);
        initialStepAlpha = 255 - finalStepAlpha;
        mView.invalidate();
    }

    public float getVisualValue(long dataValue) {
        float initialValue = (dataValue - rangeFrom.getStart()) * visualSize / rangeFrom.getSize();
        float finalValue = (dataValue - rangeTo.getStart()) * visualSize / rangeTo.getSize();
        return initialValue + (finalValue - initialValue) * elapsedTimeRatio;
    }

    public long getDataValue(float visualValue) {
        float initialValue = rangeFrom.getStart() + visualValue * rangeFrom.getSize() / visualSize ;
        float finalValue = rangeTo.getStart() + visualValue * rangeTo.getSize() / visualSize;
        return (long) (initialValue + (finalValue - initialValue) * elapsedTimeRatio);
    }
}
