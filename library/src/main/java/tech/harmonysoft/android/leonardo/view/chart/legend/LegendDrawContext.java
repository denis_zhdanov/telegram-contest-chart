package tech.harmonysoft.android.leonardo.view.chart.legend;

import tech.harmonysoft.android.leonardo.model.data.ChartDataSource;

import java.util.HashMap;
import java.util.Map;

public class LegendDrawContext {

    public final Map<ChartDataSource, Long> dataSource2dataY = new HashMap<>();

    public final int horizontalPadding;
    public final int verticalPadding;
    public int totalWidth;
    public int totalHeight;

    public float leftOnChart;
    public float topOnChart;
    public float titleHeight;

    public LegendDrawContext(int horizontalPadding, int verticalPadding) {
        this.horizontalPadding = horizontalPadding;
        this.verticalPadding = verticalPadding;
    }
}
