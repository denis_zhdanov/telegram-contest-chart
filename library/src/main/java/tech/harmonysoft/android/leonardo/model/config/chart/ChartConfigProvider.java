package tech.harmonysoft.android.leonardo.model.config.chart;

import javax.annotation.Nonnull;

public interface ChartConfigProvider {

    @Nonnull
    ChartConfig buildConfig();
}
