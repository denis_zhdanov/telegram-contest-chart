package tech.harmonysoft.android.leonardo.view.chart;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.*;
import android.util.AttributeSet;
import android.view.View;
import tech.harmonysoft.android.leonardo.model.ChartType;
import tech.harmonysoft.android.leonardo.model.Range;
import tech.harmonysoft.android.leonardo.model.axis.AxisLabelTextStrategy;
import tech.harmonysoft.android.leonardo.model.axis.LabelType;
import tech.harmonysoft.android.leonardo.model.config.chart.ChartConfig;
import tech.harmonysoft.android.leonardo.model.config.chart.ChartConfigProvider;
import tech.harmonysoft.android.leonardo.model.data.ChartDataSource;
import tech.harmonysoft.android.leonardo.model.runtime.ChartModel;
import tech.harmonysoft.android.leonardo.model.runtime.ChartModelListener;
import tech.harmonysoft.android.leonardo.model.runtime.DataMapper;
import tech.harmonysoft.android.leonardo.model.style.ReloadableStyleable;
import tech.harmonysoft.android.leonardo.model.text.TextWrapper;
import tech.harmonysoft.android.leonardo.util.LeonardoUtil;
import tech.harmonysoft.android.leonardo.view.chart.legend.LegendDrawContext;
import tech.harmonysoft.android.leonardo.view.chart.legend.LegendDrawUtil;
import tech.harmonysoft.android.leonardo.view.util.RoundedRectangleDrawer;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.*;

/**
 * @author Denis Zhdanov
 * @since 10/3/19
 */
public class ChartView extends View implements ReloadableStyleable {

    private static final Comparator<Map.Entry<ChartDataSource, Long>> SELECTION_COMPARATOR = (e1, e2) -> {
        if (e1.getValue() > e2.getValue()) {
            return -1;
        } else if (e1.getValue() < e2.getValue()) {
            return 1;
        } else {
            return 0;
        }
    };

    private final RoundedRectangleDrawer mRoundedRectangleDrawer = RoundedRectangleDrawer.INSTANCE;

    private final Map<ChartDataSource, Integer> mPercentages = new HashMap<>();

    private final Rect mRect = new Rect();

    private final TextWrapper mTextWrapper = new TextWrapper();

    private RectF mLegendRect;

    private ChartConfig         mConfig;
    private ChartConfigProvider mChartConfigProvider;
    private ChartPalette        mPalette;
    private ChartModel          mModel;
    private ChartType           mChartType;
    private ChartDrawData       mDrawData;
    private DataPointsDrawer    mDrawer;

    private final ChartModelListener mModelListener = new ChartModelListener() {
        @Override
        public void onRangeChanged(Object anchor) {
            if (anchor == getDataAnchor()) {
                invalidate();
            }
        }

        @Override
        public void onDataSourceEnabled(ChartDataSource dataSource) {
            if (mConfig != null && mConfig.isAnimationEnabled()) {
                mDrawData.fadeIn(dataSource);
                invalidate();
            }
        }

        @Override
        public void onDataSourceDisabled(ChartDataSource dataSource) {
            if (mConfig != null && mConfig.isAnimationEnabled()) {
                if (areThereOtherActiveDataSources(dataSource)) {
                    mDrawData.fadeOut(dataSource);
                }
                invalidate();
            }
        }

        @Override
        public void onDataSourceAdded(ChartDataSource dataSource) {
            if (mConfig != null && mConfig.isAnimationEnabled()) {
                if (areThereOtherActiveDataSources(dataSource)) {
                    mDrawData.fadeIn(dataSource);
                }
            }
            invalidate();
        }

        @Override
        public void onDataSourceRemoved(ChartDataSource dataSource) {
            if (mConfig != null && mConfig.isAnimationEnabled()) {
                mDrawData.fadeOut(dataSource);
            }
            invalidate();
        }

        @Override
        public void onActiveDataPointsLoaded(Object anchor) {
            if (anchor == getDataAnchor()) {
                invalidate();
            }
        }

        @Override
        public void onSelectionChange() {
            invalidate();
        }
    };

    private float mLastClickVisualX;
    private float mLastClickVisualY;

    public ChartView(Context context) {
        super(context);
        init();
    }

    public ChartView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public ChartView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    @SuppressLint("ClickableViewAccessibility")
    private void init() {
        setOnTouchListener((v, event) -> {
            mLastClickVisualX = event.getX();
            mLastClickVisualY = event.getY();
            return false;
        });
        setOnClickListener(v -> {
            if (mLegendRect != null && mModel != null && mLegendRect.contains(mLastClickVisualX, mLastClickVisualY)) {
                mModel.resetSelection();
                mLegendRect = null;
                return;
            }

            if (mConfig != null && mModel != null && mConfig.isSelectionAllowed()) {
                mModel.setSelectedX(mDrawData.visualXToDataX(mLastClickVisualX));
            }
        });
    }

    @Nonnull
    public Object getDataAnchor() {
        return this;
    }

    @Nonnull
    public DataMapper getDataMapper() {
        return mDrawData;
    }

    public float getXVisualShift() {
        return mDrawData.xAxis.visualShift;
    }

    private boolean areThereOtherActiveDataSources(ChartDataSource dataSource) {
        for (ChartDataSource source : mModel.getRegisteredDataSources()) {
            if (source != dataSource && mModel.isActive(source)) {
                return true;
            }
        }
        return false;
    }

    public void apply(ChartConfig config) {
        mConfig = config;
        mPalette = new ChartPalette(config);
        if (mModel != null) {
            mDrawData = new ChartDrawData(this, mModel, mChartType, mConfig);
            invalidate();
        }
    }

    public void apply(ChartModel chartModel, ChartType chartType) {
        if (mModel != null && mModel != chartModel) {
            mModel.removeListener(mModelListener);
        }
        mModel = chartModel;
        mChartType = chartType;
        switch (chartType) {
            case LINE_2_Y:
            case LINE:
                mDrawer = LineChartDrawer.INSTANCE;
                break;
            case STACKED_BAR:
                mDrawer = StackedBarChartDrawer.INSTANCE;
                break;
            case PERCENTAGE_STACKED:
                mDrawer = PercentageBarChartDrawer.INSTANCE;
                break;
            default:
                mDrawer = LineChartDrawer.INSTANCE;
        }
        mModel.addListener(mModelListener);

        if (mConfig != null) {
            mDrawData = new ChartDrawData(this, mModel, mChartType, mConfig);
            invalidate();
        }
    }

    public void scrollHorizontally(final float deltaVisualX) {
        float effectiveDeltaVisualX = deltaVisualX + mDrawData.xAxis.visualShift;
        mDrawData.refresh();
        Range currentXRange = mDrawData.xAxis.range;
        long deltaDataX = (long) (-effectiveDeltaVisualX / mDrawData.xAxis.unitSize);

        long minDataX = Long.MAX_VALUE;
        long maxDataX = Long.MIN_VALUE;
        for (ChartDataSource activeDataSource : mModel.getRegisteredDataSources()) {
            Range range = activeDataSource.getDataRange();
            if (range.getStart() > Long.MIN_VALUE && range.getStart() < minDataX) {
                minDataX = range.getStart();
            }
            if (range.getEnd() < Long.MAX_VALUE && range.getEnd() > maxDataX) {
                maxDataX = range.getEnd();
            }
        }

        if ((deltaDataX > 0 && currentXRange.contains(maxDataX))
            || (deltaDataX < 0 && currentXRange.contains(minDataX)))
        {
            // We can't scroll more as we're already at the min/max possible edge
            return;
        }

        mDrawData.xAxis.visualShift = effectiveDeltaVisualX % mDrawData.xAxis.unitSize;
        if (deltaDataX != 0) {
            mModel.setActiveRange(currentXRange.shift(deltaDataX), getDataAnchor());
        }
        invalidate();
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int edge = MeasureSpec.getSize(widthMeasureSpec);
        setMeasuredDimension(edge, edge);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        doDraw(canvas);
    }

    public void doDraw(Canvas canvas) {
        if (mConfig == null || mModel == null) {
            return;
        }

        mDrawData.refresh();

        drawBackground(canvas);
        drawGrid(canvas);
        mDrawer.draw(canvas, mModel, mDrawData, mPalette, mChartType, mConfig, getDataAnchor());
        drawSelection(canvas);
    }

    private void drawBackground(Canvas canvas) {
        if (mConfig.isDrawBackground()) {
            canvas.drawPaint(mPalette.getBackgroundPaint());
        }
    }

    private void drawGrid(Canvas canvas) {
        drawXAxis(canvas);
        drawYAxis(canvas);
    }

    private void drawXAxis(Canvas canvas) {
        drawXAxisLine(canvas);
        drawXAxisLabels(canvas);
    }

    private void drawXAxisLine(Canvas canvas) {
        if (!mConfig.getXAxisConfig().shouldDrawAxis()) {
            return;
        }
        float y = mDrawData.getChartBottom();
        Rect clipBounds = canvas.getClipBounds();
        if (y <= clipBounds.bottom) {
            canvas.drawLine(0, y, getWidth(), y, mPalette.getGridPaint());
        }
    }

    private void drawXAxisLabels(Canvas canvas) {
        if (!mConfig.getXAxisConfig().shouldDrawLabels()) {
            return;
        }
        Rect clipBounds = canvas.getClipBounds();
        int height = getHeight();
        int minY = height - mDrawData.xAxis.labelHeight;
        if (clipBounds.bottom <= minY) {
            return;
        }

        AxisAnimator animator = mDrawData.xAxis.animator;
        if (animator.isInProgress()) {
            drawXAxisLabels(canvas, animator.rangeFrom, animator.initialStep, animator.initialStepAlpha);
            drawXAxisLabels(canvas, mDrawData.xAxis.range, mDrawData.xAxis.axisStep, animator.finalStepAlpha);
        } else {
            drawXAxisLabels(canvas, mDrawData.xAxis.range, mDrawData.xAxis.axisStep, 255);
        }
    }

    private void drawXAxisLabels(Canvas canvas, Range range, long step, int alpha) {
        int width = getWidth();
        float unitWidth = width / ((float) range.getSize());
        Paint paint = mPalette.getXLabelPaint();
        paint.setAlpha(alpha);
        long value = range.findFirstStepValue(step);
        float x = mDrawData.xAxis.visualShift + unitWidth * (value - range.getStart());
        int y = getHeight();
        while (x >= 0 && x < width) {
            TextWrapper label = mConfig.getXAxisConfig().getLabelTextStrategy().getLabel(value, step, LabelType.NORMAL);
            canvas.drawText(label.getData(), 0, label.getLength(), x, y, paint);
            value += step;
            x += unitWidth * step;
        }
    }

    private void drawYAxis(Canvas canvas) {
        if (!mConfig.getYAxisConfig().shouldDrawAxis() || mDrawData.yAxis.range.isEmpty()) {
            return;
        }

        AxisAnimator animator = mDrawData.yAxis.animator;
        if (animator.isInProgress()) {
            drawYGrid(canvas, animator.rangeFrom, animator.initialStep, animator.initialStepAlpha);
            drawYGrid(canvas, mDrawData.yAxis.range, mDrawData.yAxis.axisStep, animator.finalStepAlpha);
        } else {
            drawYGrid(canvas, mDrawData.yAxis.range, mDrawData.yAxis.axisStep, 255);
        }
    }

    private void drawYGrid(Canvas canvas, Range range, long dataStep, int alpha) {
        boolean drawGrid = alpha > 128;
        long value = range.findFirstStepValue(dataStep);
        Paint gridPaint = mPalette.getGridPaint();
        Paint labelPaint = mPalette.getYLabelPaint();
        final long rightDataStep;
        if (mChartType == ChartType.LINE_2_Y) {
            rightDataStep = Math.round(
                    mDrawData.rightYAxis.range.getSize() * (double) mDrawData.yAxis.axisStep
                    / mDrawData.yAxis.range.getSize());
        } else {
            rightDataStep = 1L;
        }
        while (true) {
            float y = mDrawData.dataYToVisualY(value);
            if (y <= 0) {
                break;
            } else if (y <= mDrawData.getChartBottom()) {
                if (drawGrid) {
                    canvas.drawLine(0f, y, getWidth(), y, gridPaint);
                }
                TextWrapper label = mDrawData.yAxis.labelTextStrategy.getLabel(value, dataStep, LabelType.NORMAL);
                y -= mDrawData.yAxis.labelPadding;
                if (y - mDrawData.yAxis.labelHeight <= 0) {
                    break;
                }
                if (mConfig.getYAxisConfig().shouldDrawLabels()) {
                    if (mChartType == ChartType.LINE_2_Y) {
                        labelPaint.setColor(mDrawData.leftYDataSource.getLegendColor(mConfig.isLight()));
                    }
                    canvas.drawText(label.getData(), 0, label.getLength(), 0, y, labelPaint);
                    mDrawData.onYLabel(label);

                    if (mChartType == ChartType.LINE_2_Y) {
                        labelPaint.setColor(mDrawData.rightYDataSource.getLegendColor(mConfig.isLight()));
                        long rightDataY =
                                mDrawData.rightYAxis.range.getSize() * (value - mDrawData.yAxis.range.getSize())
                                / mDrawData.yAxis.range.getSize() + mDrawData.rightYAxis.range.getStart();
                        TextWrapper rightLabel = mDrawData.rightYAxis.labelTextStrategy.getLabel(
                                rightDataY, rightDataStep, LabelType.NORMAL
                        );
                        int width = mDrawData.getYLabelWidth(rightLabel);
                        canvas.drawText(rightLabel.getData(),
                                        0,
                                        rightLabel.getLength(),
                                        getWidth() - width - mDrawData.rightYAxis.labelPadding,
                                        y,
                                        labelPaint);
                        if (width + mDrawData.rightYAxis.labelPadding > mDrawData.maxRightYLabelWidth) {
                            mDrawData.maxRightYLabelWidth = width + mDrawData.rightYAxis.labelPadding;
                        }
                    }
                }
            }

            value += dataStep;
            if (value > range.getEnd()) {
                break;
            }
        }
    }

    private void drawSelection(Canvas canvas) {
        if (!mConfig.isDrawSelection() || !mModel.hasSelection()) {
            return;
        }

        LegendDrawContext context = new LegendDrawContext(mDrawData.yAxis.labelHeight * 7 / 5,
                                                          mDrawData.yAxis.labelHeight);
        LegendDrawUtil.fillValues(mModel, getDataAnchor(), context);
        mDrawer.drawSelectionOverlay(mDrawData, mModel, canvas, mConfig, mPalette);
        if (context.dataSource2dataY.isEmpty()) {
            return;
        }

        LegendDrawUtil.fillDimensionsData(context, mConfig, mDrawData, mModel, mChartType);
        mDrawer.initLegendLocation(context, mDrawData, mModel);

        drawLegend(canvas, context);
    }

    private void drawLegend(Canvas canvas, LegendDrawContext context) {
        if (mLegendRect == null) {
            mLegendRect = new RectF(context.leftOnChart,
                                    context.topOnChart,
                                    context.leftOnChart + context.totalWidth,
                                    context.topOnChart + context.totalHeight);
        } else {
            mLegendRect.set(context.leftOnChart,
                            context.topOnChart,
                            context.leftOnChart + context.totalWidth,
                            context.topOnChart + context.totalHeight);
        }

        Paint paint = mPalette.getBackgroundPaint();
        paint.setColor(mConfig.getLegendBackgroundColor());
        mRoundedRectangleDrawer.draw(mLegendRect,
                                     mPalette.getGridPaint(),
                                     paint,
                                     LeonardoUtil.DEFAULT_CORNER_RADIUS,
                                     canvas);
        drawLegendTitle(canvas, context);
        drawLegendValues(canvas, context);
    }

    private void drawLegendTitle(Canvas canvas, LegendDrawContext context) {
        TextWrapper xText = mConfig.getXAxisConfig().getLabelTextStrategy().getLabel(mModel.getSelectedX(),
                                                                                     mDrawData.xAxis.axisStep,
                                                                                     LabelType.TITLE_SMALL);
        Paint paint = mPalette.getYLabelPaint();
        paint.setColor(mConfig.getLegendTextTitleColor());
        paint.setTypeface(Typeface.DEFAULT_BOLD);
        paint.getTextBounds(xText.getData(), 0, xText.getLength(), mRect);
        context.titleHeight = mRect.height();
        canvas.drawText(xText.getData(),
                        0,
                        xText.getLength(),
                        context.leftOnChart + context.horizontalPadding,
                        context.topOnChart + context.verticalPadding + context.titleHeight,
                        paint);
        float legendRight = context.leftOnChart + context.totalWidth;
        paint.getTextBounds(">", 0, 1, mRect);
        paint.setColor(mConfig.getXAxisConfig().getLabelColor());
        canvas.drawText(">",
                        legendRight - context.horizontalPadding - mRect.width(),
                        context.topOnChart + context.verticalPadding + context.titleHeight,
                        paint);

    }

    private void drawLegendValues(Canvas canvas, LegendDrawContext context) {
        float y = context.topOnChart + context.verticalPadding * 2 + context.titleHeight;
        List<Map.Entry<ChartDataSource, Long>> data = new ArrayList<>(context.dataSource2dataY.entrySet());
        Collections.sort(data, SELECTION_COMPARATOR);
        Paint paint = mPalette.getYLabelPaint();
        AxisLabelTextStrategy textStrategy = mConfig.getYAxisConfig().getLabelTextStrategy();
        final float valueRight = context.leftOnChart + context.totalWidth - context.horizontalPadding;

        if (mChartType == ChartType.PERCENTAGE_STACKED) {
            mPercentages.clear();
            long total = 0;
            for (Long dataY : context.dataSource2dataY.values()) {
                total += dataY;
            }
            for (Map.Entry<ChartDataSource, Long> entry : context.dataSource2dataY.entrySet()) {
                mPercentages.put(entry.getKey(), Math.round(100f * entry.getValue() / total));
            }
        }

        for (Map.Entry<ChartDataSource, Long> entry : data) {
            paint.getTextBounds(entry.getKey().getLegend(), 0, entry.getKey().getLegend().length(), mRect);
            y += mRect.height();
            paint.setColor(mConfig.getLegendTextTitleColor());
            float x = context.leftOnChart + context.horizontalPadding;
            if (mChartType == ChartType.PERCENTAGE_STACKED) {
                Integer percentage = mPercentages.get(entry.getKey());
                if (percentage == null) {
                    continue;
                }
                mTextWrapper.reset();
                mTextWrapper.append(percentage);
                mTextWrapper.append("%");

                if (percentage < 10) {
                    paint.getTextBounds("0", 0, 1, mRect);
                    x += mRect.width();
                }

                canvas.drawText(mTextWrapper.getData(), 0, mTextWrapper.getLength(), x, y, paint);

                paint.getTextBounds(mTextWrapper.getData(), 0, mTextWrapper.getLength(), mRect);
                x += mRect.width() + context.horizontalPadding;
            }
            canvas.drawText(entry.getKey().getLegend(), x, y, paint);

            TextWrapper valueLabel = textStrategy.getLabel(entry.getValue(),
                                                           mDrawData.rightYDataSource == entry.getKey() ?
                                                           mDrawData.rightYAxis.axisStep : mDrawData.yAxis.axisStep,
                                                           LabelType.NORMAL);
            paint.getTextBounds(valueLabel.getData(), 0, valueLabel.getLength(), mRect);
            paint.setColor(entry.getKey().getLegendColor(mConfig.isLight()));
            canvas.drawText(valueLabel.getData(), 0, valueLabel.getLength(), valueRight - mRect.width(), y, paint);
            y += context.verticalPadding;
        }

        if (mChartType == ChartType.STACKED_BAR && context.dataSource2dataY.size() > 1) {
            long all = 0;
            for (Long value : context.dataSource2dataY.values()) {
                all += value;
            }
            paint.setColor(mConfig.getLegendTextTitleColor());
            paint.getTextBounds("All", 0, "All".length(), mRect);
            y += mRect.height();
            canvas.drawText("All", context.leftOnChart + context.horizontalPadding, y, paint);

            String allString = String.valueOf(all);
            paint.getTextBounds(allString, 0, allString.length(), mRect);
            canvas.drawText(allString, valueRight - mRect.width(), y, paint);
        }
    }

    public void setChartConfigProvider(ChartConfigProvider chartConfigProvider) {
        mChartConfigProvider = chartConfigProvider;
    }

    @Override
    public void reloadStyle() {
        if (mChartConfigProvider == null) {
            return;
        }

        apply(mChartConfigProvider.buildConfig());
    }
}
