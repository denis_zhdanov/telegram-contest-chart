package tech.harmonysoft.android.leonardo.view.navigator;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.*;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import tech.harmonysoft.android.leonardo.controller.NavigatorShowcase;
import tech.harmonysoft.android.leonardo.model.ChartType;
import tech.harmonysoft.android.leonardo.model.Range;
import tech.harmonysoft.android.leonardo.model.config.LeonardoConfigFactory;
import tech.harmonysoft.android.leonardo.model.config.axis.AxisConfig;
import tech.harmonysoft.android.leonardo.model.config.chart.ChartConfig;
import tech.harmonysoft.android.leonardo.model.config.chart.ChartConfigProvider;
import tech.harmonysoft.android.leonardo.model.config.navigator.NavigatorConfig;
import tech.harmonysoft.android.leonardo.model.config.navigator.NavigatorConfigProvider;
import tech.harmonysoft.android.leonardo.model.data.ChartDataSource;
import tech.harmonysoft.android.leonardo.model.runtime.ChartModel;
import tech.harmonysoft.android.leonardo.model.runtime.ChartModelListener;
import tech.harmonysoft.android.leonardo.model.style.ReloadableStyleable;
import tech.harmonysoft.android.leonardo.view.chart.ChartView;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

/**
 * @author Denis Zhdanov
 * @since 16/3/19
 */
public class NavigatorChartView extends View implements ReloadableStyleable {

    private static final long MIN_WIDTH_IN_PIXELS = 40;
    private static final int  CORNER_RADIUS       = 24;

    private final RectF mRect = new RectF();

    private NavigatorConfig         mConfig;
    private NavigatorConfigProvider mNavigatorConfigProvider;
    private ChartConfigProvider     mChartConfigProvider;
    private ChartModel              mModel;
    private ChartView               mView;
    private NavigatorShowcase       mShowCase;
    private ScrollListener          mScrollListener;

    private Paint mInactiveBackgroundPaint;
    private Paint mActiveBackgroundPaint;
    private Paint mActiveBorderPaint;

    private float mPreviousActionVisualX = Float.NaN;

    private ActionType mCurrentAction;

    public NavigatorChartView(Context context) {
        super(context);
        init();
    }

    public NavigatorChartView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public NavigatorChartView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        mView = new ChartView(getContext());
    }

    public void setScrollListener(ScrollListener scrollListener) {
        mScrollListener = scrollListener;
    }

    public void apply(NavigatorConfig navigatorConfig, ChartConfig chartConfig) {
        mConfig = navigatorConfig;
        mActiveBackgroundPaint = createPaint(chartConfig.getBackgroundColor());
        mInactiveBackgroundPaint = createPaint(mConfig.getInactiveChartBackgroundColor());
        mActiveBorderPaint = createPaint(mConfig.getActiveBorderColor());
        mActiveBorderPaint.setAlpha(200);
        int plotLineWidth = Math.max(1, chartConfig.getPlotLineWidthInPixels() / 2);
        AxisConfig axisConfig = LeonardoConfigFactory.newAxisConfigBuilder()
                                                     .disableLabels()
                                                     .disableAxis()
                                                     .build();
        mView.apply(LeonardoConfigFactory.newChartConfigBuilder()
                                         .withConfig(chartConfig)
                                         .withPlotLineWidthInPixels(plotLineWidth)
                                         .disableSelection()
                                         .disableBackground()
                                         .disableAnimations()
                                         .withXAxisConfig(axisConfig)
                                         .withYAxisConfig(axisConfig)
                                         .withContext(getContext())
                                         .build());

        invalidate();
    }

    public void apply(ChartModel model, ChartType chartType) {
        mModel = model;
        mView.apply(model, chartType);
        setupListener(model);
        refreshMyRange();
        invalidate();
    }

    public void apply(NavigatorShowcase showCase) {
        mShowCase = showCase;
    }

    private void setupListener(ChartModel model) {
        model.addListener(new ChartModelListener() {
            @Override
            public void onRangeChanged(Object anchor) {
                if (anchor == getModelAnchor()) {
                    refreshMyRange();
                } else if (anchor == mShowCase.getDataAnchor()) {
                    invalidate();
                }
            }

            @Override
            public void onDataSourceEnabled(ChartDataSource dataSource) {
                invalidate();
            }

            @Override
            public void onDataSourceDisabled(ChartDataSource dataSource) {
                invalidate();
            }

            @Override
            public void onDataSourceAdded(ChartDataSource dataSource) {
                invalidate();
            }

            @Override
            public void onDataSourceRemoved(ChartDataSource dataSource) {
                invalidate();
            }

            @Override
            public void onActiveDataPointsLoaded(Object anchor) {
                invalidate();
            }

            @Override
            public void onSelectionChange() {
            }
        });
    }

    private void refreshMyRange() {
        Range navigatorRange = mModel.getActiveRange(getModelAnchor());
        mModel.setActiveRange(navigatorRange, mView);
        long dependencyPointsNumber = Math.max(1, navigatorRange.getSize() / 4);
        long dependencyRangeShift = (navigatorRange.getSize() - dependencyPointsNumber) / 2;
        long dependencyRangeStart = navigatorRange.getStart() + dependencyRangeShift;
        Range dependencyRange = new Range(dependencyRangeStart,
                                          dependencyRangeStart + dependencyPointsNumber);
        mModel.setActiveRange(dependencyRange, mShowCase.getDataAnchor());
        invalidate();
    }

    @Nonnull
    private Object getModelAnchor() {
        return this;
    }

    private void mayBeInitialize() {
        mayBeSetInnerChartDimensions();
    }

    private void mayBeSetInnerChartDimensions() {
        if (mView.getWidth() != getWidth()) {
            mView.setLeft(0);
            mView.setTop(0);
            mView.setRight(getWidth());
            mView.setBottom(getHeight());
        }
    }

    @Nonnull
    private Paint createPaint(int color) {
        Paint paint = new Paint();
        paint.setColor(color);
        paint.setStyle(Paint.Style.FILL);
        return paint;
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int width = MeasureSpec.getSize(widthMeasureSpec);
        int chartHeight = getChartHeight(width);

        setMeasuredDimension(width, chartHeight);
    }

    private int getChartHeight(int width) {
        return width / 9;
    }

    @SuppressLint("WrongCall")
    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        mayBeInitialize();

        float dx = getNavigatorVisualShift();
        drawInactiveBackground(canvas, dx);
        drawActiveBackground(canvas, dx);
        mView.doDraw(canvas);
        drawActiveBorder(canvas, dx);
        drawRoundedCorners(canvas);
    }

    private void drawInactiveBackground(Canvas canvas, float dx) {
        Range activeRange = mModel.getActiveRange(mShowCase.getDataAnchor());
        if (activeRange.isEmpty()) {
            return;
        }

        Range wholeRange = mModel.getActiveRange(mView.getDataAnchor());

        if (wholeRange.getStart() < activeRange.getStart()) {
            float activeXStart = mView.getDataMapper().dataXToVisualX(activeRange.getStart());
            canvas.drawRect(0, 0, activeXStart + dx, mView.getHeight(), mInactiveBackgroundPaint);
        }

        if (wholeRange.getEnd() > activeRange.getEnd()) {
            float activeXEnd = mView.getDataMapper().dataXToVisualX(activeRange.getEnd());
            canvas.drawRect(activeXEnd + dx, 0, mView.getWidth(), mView.getHeight(), mInactiveBackgroundPaint);
        }
    }

    private void drawActiveBackground(Canvas canvas, float dx) {
        Range activeRange = mModel.getActiveRange(mShowCase.getDataAnchor());
        if (activeRange.isEmpty()) {
            return;
        }

        float activeXStart = mView.getDataMapper().dataXToVisualX(activeRange.getStart());
        float activeXEnd = mView.getDataMapper().dataXToVisualX(activeRange.getEnd());
        float radius = getBorderRadius();
        drawLeftRoundedEdge(activeXStart + dx, radius, mActiveBackgroundPaint, canvas);
        drawRightRoundedEdge(activeXEnd - radius, radius, mActiveBackgroundPaint, canvas);
        canvas.drawRect(activeXStart + dx + radius,
                        0,
                        activeXEnd + dx - radius,
                        mView.getHeight(),
                        mActiveBackgroundPaint);
    }

    private void drawActiveBorder(Canvas canvas, float dx) {
        Range activeRange = mModel.getActiveRange(mShowCase.getDataAnchor());
        if (activeRange.isEmpty()) {
            return;
        }
        int borderWidth = mConfig.getActiveBorderHorizontalWidthInPixels();
        float radius = getBorderRadius();
        int height = getHeight();
        float borderMarkerWidth = getBorderMarkerWidth();

        // Left edge
        float activeXStart = mView.getDataMapper().dataXToVisualX(activeRange.getStart());
        drawLeftRoundedEdge(activeXStart + dx - borderWidth, radius, mActiveBorderPaint, canvas);
        canvas.drawRect(activeXStart - radius + dx,
                        0,
                        activeXStart + dx,
                        height,
                        mActiveBorderPaint);
        drawBorderMarker(activeXStart - borderWidth + (borderWidth - borderMarkerWidth) / 2f, canvas);

        // Right edge
        float activeXEnd = mView.getDataMapper().dataXToVisualX(activeRange.getEnd());
        drawRightRoundedEdge(activeXEnd + radius + dx, radius, mActiveBorderPaint, canvas);
        canvas.drawRect(activeXEnd + dx,
                        0,
                        activeXEnd + dx + radius,
                        mView.getHeight(),
                        mActiveBorderPaint);
        drawBorderMarker(activeXEnd + (borderWidth - borderMarkerWidth) / 2f, canvas);

        // Top edge
        canvas.drawRect(activeXStart + dx - radius,
                        0,
                        activeXEnd + dx + radius,
                        mConfig.getActiveBorderVerticalHeightInPixels(),
                        mActiveBorderPaint);

        // Bottom edge
        canvas.drawRect(activeXStart + dx - radius,
                        mView.getHeight(),
                        activeXEnd + dx + radius,
                        mView.getHeight() - mConfig.getActiveBorderVerticalHeightInPixels(),
                        mActiveBorderPaint);
    }

    private float getBorderRadius() {
        return mConfig.getActiveBorderHorizontalWidthInPixels() / 2f;
    }

    private void drawLeftRoundedEdge(float left, float radius, Paint paint, Canvas canvas) {
        int height = getHeight();

        // Top rounded corner
        mRect.set(left, 0, left + 2 * radius, 2 * radius);
        canvas.drawArc(mRect, 180, 90, true, paint);

        // Bottom rounded corner
        mRect.set(left, height - 2 * radius, left + 2 * radius, height);
        canvas.drawArc(mRect, 90, 90, true, paint);

        // Middle rect
        canvas.drawRect(left, radius, left + radius, height - radius, paint);
    }

    private void drawRightRoundedEdge(float left, float radius, Paint paint, Canvas canvas) {
        int height = getHeight();

        // Top rounded corner
        mRect.set(left - radius, 0, left + radius, 2 * radius);
        canvas.drawArc(mRect, 270, 90, true, paint);

        // Bottom rounded corner
        mRect.set(left - radius, height - 2 * radius, left + radius, height);
        canvas.drawArc(mRect, 0, 90, true, paint);

        // Middle rect
        canvas.drawRect(left, radius, left + radius, height - radius, paint);
    }

    private void drawBorderMarker(float left, Canvas canvas) {
        int colorToRestore = mActiveBackgroundPaint.getColor();
        mActiveBackgroundPaint.setColor(Color.WHITE);

        float radius = getBorderMarkerWidth() / 2f;
        int height = getHeight();
        float markerHeight = height / 4f;
        float markerTop = (height - markerHeight) / 2f;
        float markerBottom = height - markerTop;
        canvas.drawCircle(left + radius, markerTop + radius, radius, mActiveBackgroundPaint);
        canvas.drawCircle(left + radius, markerBottom - radius, radius, mActiveBackgroundPaint);
        canvas.drawRect(left, markerTop + radius, left + 2 * radius, markerBottom - radius, mActiveBackgroundPaint);

        mActiveBackgroundPaint.setColor(colorToRestore);
    }

    private float getBorderMarkerWidth() {
        return mConfig.getActiveBorderHorizontalWidthInPixels() / 5f;
    }

    private void drawRoundedCorners(Canvas canvas) {
        int width = getWidth();
        int height = getHeight();
        Path path;

        // Top-left
        path = new Path();
        mRect.set(0, 0, CORNER_RADIUS * 2, CORNER_RADIUS * 2);
        path.arcTo(mRect, 180, 90);
        path.lineTo(0, 0);
        path.lineTo(0, CORNER_RADIUS);
        canvas.drawPath(path, mActiveBackgroundPaint);

        // Top-right
        path = new Path();
        mRect.set(width - 2 * CORNER_RADIUS, 0, width, CORNER_RADIUS * 2);
        path.arcTo(mRect, 270, 90);
        path.lineTo(width, 0);
        path.lineTo(width - CORNER_RADIUS, 0);
        canvas.drawPath(path, mActiveBackgroundPaint);

        // Bottom-right
        path = new Path();
        mRect.set(width - 2 * CORNER_RADIUS, height - 2 * CORNER_RADIUS, width, height);
        path.arcTo(mRect, 0, 90);
        path.lineTo(width, height);
        path.lineTo(width, height - CORNER_RADIUS);
        canvas.drawPath(path, mActiveBackgroundPaint);

        // Bottom-left
        path = new Path();
        mRect.set(0, height - CORNER_RADIUS * 2, CORNER_RADIUS * 2, height);
        path.arcTo(mRect, 90, 90);
        path.lineTo(0, height);
        path.lineTo(CORNER_RADIUS, height);
        canvas.drawPath(path, mActiveBackgroundPaint);
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        int action = event.getActionMasked();
        switch (action) {
            case MotionEvent.ACTION_DOWN:
                startAction(event.getX());
                break;
            case MotionEvent.ACTION_MOVE:
                move(event.getX());
                break;
            case MotionEvent.ACTION_UP:
                release(event.getX());
                break;
        }
        return true;
    }

    private void startAction(float visualX) {
        mCurrentAction = getActionType(visualX);
        if (mCurrentAction != null) {
            mPreviousActionVisualX = visualX;
            if (mScrollListener != null) {
                mScrollListener.onStarted();
            }
            invalidate();
        }
    }

    public void move(final float visualX) {
        if (mCurrentAction == null || Float.isNaN(mPreviousActionVisualX)) {
            return;
        }

        float navigatorVisualDeltaX = mPreviousActionVisualX - visualX;
        if (navigatorVisualDeltaX == 0) {
            return;
        }

        mPreviousActionVisualX = visualX;

        float ratio = getShowCaseVisualRatio();

        if (mCurrentAction == ActionType.MOVE_COMPLETE_ACTIVE_INTERVAL) {
            mShowCase.scrollHorizontally(navigatorVisualDeltaX * ratio);
            invalidate();
        } else if (mCurrentAction == ActionType.MOVE_ACTIVE_INTERVAL_START) {
            Range showCaseDataRange = mModel.getActiveRange(mShowCase.getDataAnchor());
            float endRangeVisualX = mView.getDataMapper().dataXToVisualX(showCaseDataRange.getEnd());
            if (endRangeVisualX - visualX < MIN_WIDTH_IN_PIXELS) {
                // Don't allow selector to become too narrow
                return;
            }
            long newStartDataX = mView.getDataMapper().visualXToDataX(Math.max(visualX, 0));
            if (newStartDataX != showCaseDataRange.getStart()) {
                mModel.setActiveRange(new Range(newStartDataX, showCaseDataRange.getEnd()), mShowCase.getDataAnchor());
            }
        } else {
            Range showCaseDataRange = mModel.getActiveRange(mShowCase.getDataAnchor());
            Range myDataRange = mModel.getActiveRange(getModelAnchor());
            float startRangeVisualX = mView.getDataMapper().dataXToVisualX(showCaseDataRange.getStart());
            if (visualX - startRangeVisualX < MIN_WIDTH_IN_PIXELS) {
                // Don't allow selector to become too narrow
                return;
            }
            long newEndDataX = mView.getDataMapper().visualXToDataX(Math.max(visualX, 0));
            if (newEndDataX <= myDataRange.getEnd() && newEndDataX != showCaseDataRange.getEnd()) {
                mModel.setActiveRange(new Range(showCaseDataRange.getStart(), newEndDataX), mShowCase.getDataAnchor());
            }
        }
    }

    private void release(float visualX) {
        move(visualX);
        stopAction();
    }

    public void stopAction() {
        mCurrentAction = null;
        mPreviousActionVisualX = Float.NaN;
        if (mScrollListener != null) {
            mScrollListener.onStopped();
        }
        invalidate();
    }

    @Nullable
    private ActionType getActionType(float x) {
        Range activeRange = mModel.getActiveRange(mShowCase.getDataAnchor());
        if (activeRange.isEmpty()) {
            return null;
        }

        float dx = getNavigatorVisualShift();
        float startX = mView.getDataMapper().dataXToVisualX(activeRange.getStart()) + dx;
        float endX = mView.getDataMapper().dataXToVisualX(activeRange.getEnd()) + dx;

        int borderWidth = mConfig.getActiveBorderHorizontalWidthInPixels();

        if (x >= startX - borderWidth && x <= startX) {
            return ActionType.MOVE_ACTIVE_INTERVAL_START;
        }

        if (x > startX && x < endX) {
            return ActionType.MOVE_COMPLETE_ACTIVE_INTERVAL;
        }

        if (x >= endX && x <= endX + borderWidth) {
            return ActionType.MOVE_ACTIVE_INTERVAL_END;
        }

        return null;
    }

    private float getShowCaseVisualRatio() {
        Range navigatorDataRange = mModel.getActiveRange(getModelAnchor());
        Range showCaseDataRange = mModel.getActiveRange(mShowCase.getDataAnchor());
        return ((float) navigatorDataRange.getSize()) / (showCaseDataRange.getSize());
    }

    private float getNavigatorVisualShift() {
        float dx = 0;
        float showCaseVisualXShift = mShowCase.getVisualXShift();
        if (showCaseVisualXShift != 0) {
            dx = showCaseVisualXShift / getShowCaseVisualRatio();
        }
        return -dx;
    }

    public void setNavigatorConfigProvider(NavigatorConfigProvider navigatorConfigProvider) {
        mNavigatorConfigProvider = navigatorConfigProvider;
    }

    public void setChartConfigProvider(ChartConfigProvider chartConfigProvider) {
        mChartConfigProvider = chartConfigProvider;
    }

    @Override
    public void reloadStyle() {
        if (mNavigatorConfigProvider != null && mChartConfigProvider != null) {
            apply(mNavigatorConfigProvider.buildConfig(), mChartConfigProvider.buildConfig());
        }
    }

    private enum ActionType {
        MOVE_COMPLETE_ACTIVE_INTERVAL, MOVE_ACTIVE_INTERVAL_START, MOVE_ACTIVE_INTERVAL_END
    }
}
