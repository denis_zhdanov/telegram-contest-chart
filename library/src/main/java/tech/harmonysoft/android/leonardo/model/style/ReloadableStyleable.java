package tech.harmonysoft.android.leonardo.model.style;

public interface ReloadableStyleable {

    void reloadStyle();
}
