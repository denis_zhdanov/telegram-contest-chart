package tech.harmonysoft.android.leonardo.view.chart;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import tech.harmonysoft.android.leonardo.model.ChartType;
import tech.harmonysoft.android.leonardo.model.DataPoint;
import tech.harmonysoft.android.leonardo.model.VisualPoint;
import tech.harmonysoft.android.leonardo.model.config.chart.ChartConfig;
import tech.harmonysoft.android.leonardo.model.data.ChartDataSource;
import tech.harmonysoft.android.leonardo.model.runtime.ChartModel;
import tech.harmonysoft.android.leonardo.util.IterableEx;
import tech.harmonysoft.android.leonardo.util.LineUtil;
import tech.harmonysoft.android.leonardo.view.chart.legend.LegendDrawContext;

import java.util.*;

import static tech.harmonysoft.android.leonardo.util.LongPropertyUtil.equalOrLower;
import static tech.harmonysoft.android.leonardo.util.LongPropertyUtil.next;

public class LineChartDrawer implements DataPointsDrawer {

    public static final LineChartDrawer INSTANCE = new LineChartDrawer();

    private final IterableEx<DataPoint> mIterableEx = new IterableEx<>();

    @Override
    public void draw(Canvas canvas,
                     ChartModel model,
                     ChartDrawData drawData,
                     ChartPalette palette,
                     ChartType chartType,
                     ChartConfig config,
                     Object dataAnchor)
    {
        for (ChartDataSource dataSource : model.getRegisteredDataSources()) {
            drawPlot(canvas, model, drawData, palette, chartType, dataSource, dataAnchor, config.isLight());
        }
        drawSelection(model, drawData, palette, canvas, config, dataAnchor, config.isLight());
    }

    private void drawPlot(Canvas canvas,
                          ChartModel model,
                          ChartDrawData drawData,
                          ChartPalette palette,
                          ChartType chartType,
                          ChartDataSource dataSource,
                          Object dataAnchor,
                          boolean lightTheme)
    {
        Paint paint = palette.getPlotPaint();
        paint.setColor(dataSource.getPlotColor(lightTheme));
        if (drawData.isAnimationInProgress(dataSource)) {
            paint.setAlpha(drawData.getCurrentAlpha(dataSource));
        } else if (!model.isActive(dataSource)) {
            return;
        }



        float minX = drawData.getChartLeft();
        float maxX = drawData.getChartRight();

        DataPoint previous = model.getPreviousPointForActiveRange(dataSource, dataAnchor);
        DataPoint next = model.getNextPointForActiveRange(dataSource, dataAnchor);
        NavigableSet<DataPoint> interval = model.getCurrentRangePoints(dataSource, dataAnchor);
        mIterableEx.refresh(interval, previous, next);

        Path path = new Path();
        boolean first = true;
        boolean stop = false;
        float prevVisualX = 0f;
        float prevVisualY = 0f;
        for (DataPoint point : mIterableEx) {
            if (stop) {
                break;
            }
            float visualX = drawData.dataXToVisualX(point.getX());
            float visualY = drawData.dataYToVisualY(point.getY());
            if (chartType == ChartType.LINE_2_Y && dataSource == drawData.rightYDataSource) {
                visualY = drawData.dataYToVisualY(point.getY(), dataSource);
            }
            if (mIterableEx.isFirst(point)) {
                prevVisualX = visualX;
                prevVisualY = visualY;
                continue;
            }

            if (prevVisualX < minX && visualX <= minX) {
                prevVisualX = visualX;
                prevVisualY = visualY;
                continue;
            }

            float x;
            float y;
            if (prevVisualX < minX) {
                x = minX;
                y = LineUtil.getY(prevVisualX, prevVisualY, visualX, visualY, x);
                if (y > drawData.getChartBottom()) {
                    y = drawData.getChartBottom();
                    x = LineUtil.getX(prevVisualX, prevVisualY, visualX, visualY, y);
                }
            } else if (visualX > maxX) {
                if (first) {
                    first = false;
                    path.moveTo(prevVisualX, prevVisualY);
                } else {
                    path.lineTo(prevVisualX, prevVisualY);
                }
                x = maxX;
                y = LineUtil.getY(prevVisualX, prevVisualY, visualX, visualY, x);
                if (y > drawData.getChartBottom()) {
                    y = drawData.getChartBottom();
                    x = LineUtil.getX(prevVisualX, prevVisualY, visualX, visualY, y);
                }
                stop = true;
            } else {
                x = prevVisualX;
                y = prevVisualY;
            }

            if (first) {
                first = false;
                path.moveTo(x, y);
            } else {
                path.lineTo(x, y);
            }

            if (!stop && mIterableEx.isLast(point)) {
                float endX;
                float endY;
                if (visualX <= maxX) {
                    endX = visualX;
                    endY = visualY;
                } else {
                    endX = maxX;
                    endY = LineUtil.getY(prevVisualX, prevVisualY, visualX, visualY, endX);
                    if (endY > drawData.getChartBottom()) {
                        endY = drawData.getChartBottom();
                        endX = LineUtil.getX(prevVisualX, prevVisualY, visualX, visualY, endY);
                    }
                }
                path.lineTo(endX, endY);
            }

            prevVisualX = visualX;
            prevVisualY = visualY;
        }

        canvas.drawPath(path, paint);
    }

    private void drawSelection(ChartModel model,
                               ChartDrawData drawData,
                               ChartPalette palette,
                               Canvas canvas,
                               ChartConfig config,
                               Object dataAnchor,
                               boolean lightTheme)
    {
        if (!model.hasSelection()) {
            return;
        }

        long dataX = model.getSelectedX();
        float visualX = drawData.dataXToVisualX(dataX);
        if (visualX < drawData.getChartLeft() || visualX > drawData.getChartRight()) {
            return;
        }

        canvas.drawLine(visualX, drawData.getChartBottom(), visualX, 0f, palette.getGridPaint());

        for (ChartDataSource dataSource : model.getRegisteredDataSources()) {
            if (!model.isActive(dataSource)) {
                continue;
            }
            NavigableSet<DataPoint> points = model.getCurrentRangePoints(dataSource, dataAnchor);
            if (points.isEmpty()) {
                continue;
            }

            long dataY;

            DataPoint firstDataPoint = points.first();
            DataPoint lastDataPoint = points.last();
            if (dataX < firstDataPoint.getX()) {
                DataPoint previousDataPoint = model.getPreviousPointForActiveRange(dataSource, dataAnchor);
                if (previousDataPoint == null) {
                    continue;
                }
                dataY = (long) LineUtil.getY(previousDataPoint.getX(),
                                             previousDataPoint.getY(),
                                             firstDataPoint.getX(),
                                             firstDataPoint.getY(),
                                             dataX);
            } else if (dataX > lastDataPoint.getX()) {
                DataPoint nextDataPoint = model.getNextPointForActiveRange(dataSource, dataAnchor);
                if (nextDataPoint == null) {
                    continue;
                }
                dataY = (long) LineUtil.getY(lastDataPoint.getX(),
                                             lastDataPoint.getY(),
                                             nextDataPoint.getX(),
                                             nextDataPoint.getY(),
                                             dataX);
            } else {
                DataPoint equalOrLower = equalOrLower(dataX, points);
                if (equalOrLower == null) {
                    continue;
                }
                if (equalOrLower.getX() == dataX) {
                    dataY = equalOrLower.getY();
                } else {
                    DataPoint next = next(dataX, points);
                    if (next == null) {
                        continue;
                    }
                    dataY = (long) LineUtil.getY(equalOrLower.getX(),
                                                 equalOrLower.getY(),
                                                 next.getX(),
                                                 next.getY(),
                                                 dataX);
                }
            }

            VisualPoint visualPoint = drawData.dataPointToVisualPoint(new DataPoint(dataX, dataY));
            int yShift = config.getPlotLineWidthInPixels() / 2;
            drawSelectionPlotSign(palette,
                                  config,
                                  canvas,
                                  new VisualPoint(visualPoint.getX(), visualPoint.getY() - yShift),
                                  dataSource.getPlotColor(lightTheme));
        }
    }

    private void drawSelectionPlotSign(ChartPalette palette,
                                       ChartConfig config,
                                       Canvas canvas,
                                       VisualPoint point,
                                       int color)
    {
        Paint paint = palette.getPlotPaint();
        paint.setColor(config.getBackgroundColor());
        paint.setStyle(Paint.Style.FILL);
        canvas.drawCircle(point.getX(), point.getY(), config.getSelectionSignRadiusInPixels(), paint);

        paint.setColor(color);
        paint.setStyle(Paint.Style.STROKE);
        canvas.drawCircle(point.getX(), point.getY(), config.getSelectionSignRadiusInPixels(), paint);
    }

    @Override
    public void initLegendLocation(LegendDrawContext context, ChartDrawData drawData, ChartModel model) {
        final float selectionVisualX = drawData.dataXToVisualX(model.getSelectedX());

        // Ideally we'd like to show legend above selection
        List<Float> visualYs = new ArrayList<>();
        for (Map.Entry<ChartDataSource, Long> entry : context.dataSource2dataY.entrySet()) {
            visualYs.add(drawData.dataYToVisualY(entry.getValue(), entry.getKey()));
        }
        Collections.sort(visualYs);
        float top = 0f;
        for (Float visualY : visualYs) {
            if (visualY - top >= context.totalHeight) {
                context.topOnChart = (visualY - top - context.totalHeight) / 2 + top;
                context.leftOnChart = Math.max(0, selectionVisualX - context.totalWidth / 2f);
                if (context.leftOnChart + context.totalWidth > drawData.getChartRight()) {
                    context.leftOnChart = drawData.getChartRight() - context.totalWidth - context.horizontalPadding;
                }
                return;
            } else {
                top = visualY;
            }
        }

        // We can't show legend above the selected X without hiding one or more selection points.

        if (drawData.getChartRight() - selectionVisualX >= context.totalWidth) {
            // We can show legend to the right of selected x
            context.leftOnChart = selectionVisualX + context.horizontalPadding;
            context.topOnChart = visualYs.get(0);
            if (context.topOnChart + context.totalHeight > drawData.getChartBottom()) {
                context.topOnChart = drawData.getChartBottom() - context.verticalPadding - context.totalHeight;
            }
            return;
        }

        if (selectionVisualX >= context.totalWidth) {
            // We can show legend to the left of selected x
            context.leftOnChart = selectionVisualX - context.horizontalPadding - context.totalWidth;
            context.topOnChart = visualYs.get(0);
            if (context.topOnChart + context.totalHeight > drawData.getChartBottom()) {
                context.topOnChart = drawData.getChartBottom() - context.verticalPadding - context.totalHeight;
            }
            return;
        }

        // We failed finding a location where legend doesn't hide selection points, let's show it above them.
        context.leftOnChart = drawData.getChartLeft()
                              + (drawData.getChartRight() - drawData.getChartLeft() - context.totalWidth) / 2f;
        context.topOnChart = (drawData.getChartBottom() - context.totalHeight) / 2f;
    }

    @Override
    public void drawSelectionOverlay(ChartDrawData drawData,
                                     ChartModel model,
                                     Canvas canvas,
                                     ChartConfig config,
                                     ChartPalette palette)
    {
    }
}
